import numpy as np
import nibabel as nib
import struct


def write_slice():
    imf = nib.load('/home/domen/Registracija/Slike/5_MR_T1.nii.gz')
    a = imf.get_data()
    pdim = imf.header.get_zooms()  # pixel dimensions
    aslice = np.flipud(a[144, :, :].transpose())
    b = aslice.flatten()
    with open('/home/domen/Peskovnik/bark/mrslice.lch', 'wb') as f:
        f.write(struct.pack('>i', aslice.shape[0]))
        f.write(struct.pack('>i', aslice.shape[1]))
        f.write(struct.pack('>d', pdim[2]))
        f.write(struct.pack('>d', pdim[1]))
        for v in b:
            f.write(struct.pack('>h', v))


def write_image():
    imf = nib.load('/home/domen/Registracija/Slike/5_MR_T1.nii.gz')
    a = imf.get_data()
    pdim = imf.header.get_zooms()  # pixel dimensions
    b = a.flatten()
    with open('/home/domen/Peskovnik/bark/mr_image.lch', 'wb') as f:
        f.write(struct.pack('>i', a.shape[0]))
        f.write(struct.pack('>i', a.shape[1]))
        f.write(struct.pack('>i', a.shape[2]))
        f.write(struct.pack('>d', pdim[0]))
        f.write(struct.pack('>d', pdim[1]))
        f.write(struct.pack('>d', pdim[2]))
        for v in b:
            f.write(struct.pack('>h', v))


write_image()

# Useful documentation:
# https://docs.python.org/3/library/struct.html <- for the use of the struct.pack and its encoding
# https://stackoverflow.com/questions/20955543/python-writing-binary <- answer of Hyperboreus
