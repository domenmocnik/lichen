package io;

import gridfun.GridFunction2D;
import gridfun.GridFunction3D;

import java.io.*;

/**
 * 2-dimensional image input/output.
 */
public class Image2D {

    public static GridFunction2D readLCH(String filename) {
        try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)))) {
            int sizeX = dis.readInt();
            int sizeY = dis.readInt();
            int size = sizeX * sizeY;
            double spacingX = dis.readDouble();
            double spacingY = dis.readDouble();
            double[] values = new double[size];
            for (int i = 0; i < size; i++) {
                values[i] = (double) dis.readUnsignedShort();
            }
            dis.close();
            return new GridFunction2D(sizeX, sizeY, values, spacingX, spacingY);
        } catch (EOFException e) {
            throw new RuntimeException("We reached end of file, but we should not?");
        } catch (IOException e) {
            throw new RuntimeException("Error while reading input image.");
        }
    }

    public static GridFunction3D read3DImageFromLCH(String filename) {
        try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)))) {
            int sizeX = dis.readInt();
            int sizeY = dis.readInt();
            int sizeZ = dis.readInt();
            int size = sizeX * sizeY * sizeZ;
            double spacingX = dis.readDouble();
            double spacingY = dis.readDouble();
            double spacingZ = dis.readDouble();
            GridFunction3D g = new GridFunction3D(sizeX, sizeY, sizeZ, spacingX, spacingY, spacingZ);
            double[] values = g.values;
            for (int i = 0; i < size; i++) {
                values[i] = (double) dis.readShort(); // was readUnsignedShort() before
            }
            dis.close();
            return g;
        } catch (EOFException e) {
            throw new RuntimeException("We reached end of file, but we should not?");
        } catch (IOException e) {
            throw new RuntimeException("Error while reading input image.");
        }
    }
}
