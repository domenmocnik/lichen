package optim;

import costfun.CostFunction;
import util.ValueGradientPair;

import java.util.Arrays;
import java.util.function.DoubleUnaryOperator;

/**
 * Created by domen on 29.8.2017.
 */
public class GradientDescent { // TODO: this class is a stub

    private final CostFunction fun;
    private final double tol = 1e-8;
    private final int maxIter = 100;

    public GradientDescent(CostFunction function) {
        this.fun = function;
    }

    double[] optimise(double[] x0) {
        final int p = x0.length;
        double[] theta = Arrays.copyOf(x0, p);
        double dif = Double.MAX_VALUE;
        int iter = 0;
        while (dif > tol && iter <= maxIter) {
            ValueGradientPair vgp = fun.getValueAndGradientAt(theta);
            double step = goldenSectionSearch(t -> {
                double[] w = new double[p];
                for (int i = 0; i < p; i++) {
                    w[i] = theta[i] - t * vgp.gradient[i];
                }
                return fun.getValueAt(w);
            }, 0, 100, 1e-8);
            dif = 0;
            for (int i = 0; i < p; i++) {
                double thetaNew = theta[i] - step * vgp.gradient[i];
                double d = thetaNew - theta[i];
                dif += d * d;
                theta[i] = thetaNew;
            }
            dif = Math.sqrt(dif);
            iter++;
            System.out.println(iter + ": value: " + vgp.value + ", point: " + Arrays.toString(theta) + ", dif: " + dif + ", grad: " + Arrays.toString(vgp.gradient));
        }
        return theta;
    }




    private static final double PHI = (Math.sqrt(5d) + 1d) / 2d; // golden ratio

    /**
     * Performs golden-section search to find minimum of the given univariate scalar function.
     * @param f Function f: R -> R
     * @param a Lower bound of search interval.
     * @param b Upper bound of search interval.
     * @param tolerance Tolerance.
     * @return A point on the interval [a, b], where f reaches its local minimum.
     */
    private static double goldenSectionSearch(DoubleUnaryOperator f, double a, double b, double tolerance) {
        double c = b - (b - a) / PHI;
        double d = a + (b - a) / PHI;
        while (Math.abs(c - d) > tolerance) {
            if (f.applyAsDouble(c) < f.applyAsDouble(d)) {
                b = d;
            } else {
                a = c;
            }
            c = b - (b - a) / PHI;
            d = a + (b - a) / PHI;
        }
        return (b + a) / 2;
    }

}
