package optim;

import costfun.CostFunction;
import util.Ops;

import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;

/**
 * Non-linear conjugate gradient method for optimisation of differentiable multivariate scalar function.
 */
public class ConjugateGradientMethod {

    private final CostFunction fun;
    private final double tol = 1e-8;
    private final int maxIter = 100;

    /**
     * Constructs method for optimisation of given cost function with non-linear conjugate gradients.
     * @param function Cost function to be minimised.
     */
    public ConjugateGradientMethod(CostFunction function) {
        this.fun = function;
    }

    /**
     * Tries to find local minimum of cost function from initial approximation.
     * @param x0 Initial approximation.
     * @return Point where cost function reaches its local minimum.
     */
    public double[] optimise(double[] x0) {
        final int P = x0.length;
        final double[] x = new double[P];
        System.arraycopy(x0, 0, x, 0, P);
        final double[] g = new double[P];
        final double[] g0 = new double[P];
        final double[] s = new double[P];
        final double rep = P + 1;

        int iter = 0; // number of iterations
        double res = Double.MAX_VALUE; // residuum ||x - x0||^2

        while(res > tol && iter <= maxIter) {
            double[] gf = fun.getValueAndGradientAt(x).gradient;
            for (int i = 0; i < P; i++) { g[i] = -gf[i]; }
            if (iter % rep == 0) {
                System.arraycopy(g, 0, s, 0, P);
            } else {
                double beta = Ops.dot(g, g) / Ops.dot(g0, g0); // Fletcher-Reeves //TODO: Ostale bete & kaj če je beta<0
                for (int i = 0; i < P; i++) { s[i] = g[i] + beta * s[i]; }
            }
            double alpha = goldenSectionSearch(lineSearchFunction(x, s), 0, 1, 1e-8);
            res = 0;
            for (int i = 0; i < P; i++) {
                double xn = x[i] + alpha * s[i];
                double d = xn - x[i];
                res += d * d;
                x[i] = xn;
                g0[i] = g[i];
            }
            res = Math.sqrt(res);
            iter++;
        }
        return x;
    }

    /**
     * Tries to find local minimum of cost function from initial approximation. This method uses provided diagonal
     * precondition matrix to accelerate convergence.
     * @param x0 Initial approximation.
     * @param precond Array with diagonal elements of precondition matrix.
     * @param resetNum Number which tells after how many iterations the method should restart.
     * @return Point where cost function reaches its local minimum.
     */
    public double[] optimise(double[] x0, double[] precond, int resetNum) {
        final int p = x0.length;
        final double[] x = new double[p];
        System.arraycopy(x0, 0, x, 0, p);
        final double[] g = new double[p];
        final double[] g0 = new double[p];
        final double[] s = new double[p];

        int iter = 0; // current iteration
        double res = Double.MAX_VALUE; // residuum ||x - x0||^2

        while(res > tol && iter < maxIter) {
            double[] gf = fun.getValueAndGradientAt(x).gradient;
            for (int i = 0; i < p; i++) { g[i] = - gf[i] * precond[i]; }
            if (iter % resetNum == 0) {
                System.arraycopy(g, 0, s, 0, p);
            } else {
                double beta = Ops.dot(g, g) / Ops.dot(g0, g0); // Fletcher-Reeves //TODO: Ostale bete & kaj če je beta<0
                for (int i = 0; i < p; i++) { s[i] = g[i] + beta * s[i]; }
            }
            double alpha = goldenSectionSearch(lineSearchFunction(x, s), 0, 1, 1e-8);
            res = 0;
            for (int i = 0; i < p; i++) {
                double xn = x[i] + alpha * s[i];
                double d = xn - x[i];
                res += d * d;
                x[i] = xn;
                g0[i] = g[i];
            }
            res = Math.sqrt(res);
            iter++;
        }
        return x;
    }

    /**
     * Tries to find local minimum of cost function from initial approximation. This method uses provided supplier for
     * diagonal precondition matrix to accelerate convergence.
     * @param x0 Initial approximation.
     * @param preconditionSupplier Supplier of arrays with diagonal elements of precondition matrix.
     * @param resetNum Number which tells after how many iterations the method should restart.
     * @return Point where cost function reaches its local minimum.
     */
    public double[] optimise(double[] x0, Supplier<double[]> preconditionSupplier, int resetNum) {
        final int p = x0.length;
        final double[] x = new double[p];
        System.arraycopy(x0, 0, x, 0, p);
        final double[] g = new double[p];
        final double[] g0 = new double[p];
        final double[] s = new double[p];
        double[] precond = {0}; // Just to satisfy the compiler; it will be initialized at the start of the while loop.

        int iter = 0; // current iteration
        double res = Double.MAX_VALUE; // residuum ||x - x0||^2

        while(res > tol && iter < maxIter) {
            boolean reset = iter % resetNum == 0;
            if (reset) { precond = preconditionSupplier.get(); } // updating the precondition matrix
            double[] gf = fun.getValueAndGradientAt(x).gradient;
            for (int i = 0; i < p; i++) { g[i] = - gf[i] * precond[i]; }
            if (reset) {
                System.arraycopy(g, 0, s, 0, p);
            } else {
                double beta = Ops.dot(g, g) / Ops.dot(g0, g0); // Fletcher-Reeves //TODO: Ostale bete & kaj če je beta<0
                for (int i = 0; i < p; i++) { s[i] = g[i] + beta * s[i]; }
            }
            double alpha = goldenSectionSearch(lineSearchFunction(x, s), 0, 1, 1e-8);
            res = 0;
            for (int i = 0; i < p; i++) {
                double xn = x[i] + alpha * s[i];
                double d = xn - x[i];
                res += d * d;
                x[i] = xn;
                g0[i] = g[i];
            }
            res = Math.sqrt(res);
            iter++;
        }
        return x;
    }

    /**
     * Creates scalar function α -> = this.fun(x + α * d) from given vectors x and d.
     * @param x Approximation vector.
     * @param d Direction vector.
     * @return Scalar function α -> this.fun(x + α * d).
     */
    private DoubleUnaryOperator lineSearchFunction(double[] x, double[] d) {
        return t -> {
            double[] w = new double[x.length];
            for (int i = 0; i < x.length; i++) {
                w[i] = x[i] + t * d[i];
            }
            return fun.getValueAt(w);
        };
    }


    private static final double PHI = (Math.sqrt(5d) + 1d) / 2d; // golden ratio

    /**
     * Performs golden-section search to find minimum of the given univariate scalar function.
     * @param f Function f: R -> R
     * @param a Lower bound of search interval.
     * @param b Upper bound of search interval.
     * @param tolerance Tolerance.
     * @return A point on the interval [a, b], where f reaches its local minimum.
     */
    private static double goldenSectionSearch(DoubleUnaryOperator f, double a, double b, double tolerance) {
        double c = b - (b - a) / PHI;
        double d = a + (b - a) / PHI;
        while (Math.abs(c - d) > tolerance) {
            if (f.applyAsDouble(c) < f.applyAsDouble(d)) {
                b = d;
            } else {
                a = c;
            }
            c = b - (b - a) / PHI;
            d = a + (b - a) / PHI;
        }
        return (b + a) / 2;
    }

}
