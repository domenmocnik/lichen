package optim;

import costfun.CostFunction;
import org.apache.commons.math3.optim.*;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunctionGradient;
import org.apache.commons.math3.optim.nonlinear.scalar.gradient.NonLinearConjugateGradientOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.gradient.Preconditioner;

/**
 * A wrapper for Apache-commons-math implementation of non-linear conjugate gradient optimisation method.
 */
public class AcmConjugateGradientMethod {

    private CostFunction fun; // the function to be optimised
    private double[] initialGuess; // initial guess: point, where iteration starts
    private int maxIter; // maximum number of iterations
    private int maxEval; // maximum number of function evaluations
    private GoalType goalType; // goal of optimisation (minimization or maximization)
    private double convergenceRelativeTolerance; // relative tolerance in convergence checker
    private double convergenceAbsoluteTolerance; // absolute tolerance in convergence checker
    private NonLinearConjugateGradientOptimizer.Formula formula; // update formula: Polak-Ribiere or Fletcher-Reeves
    private double lineSearchRelativeTolerance;
    private double lineSearchAbsoluteTolerance;
    private double lineSearchBracketingRange;
    private Preconditioner preconditioner; // preconditioner

    /**
     * Instantiates conjugate gradient method with default values for optimisation of provided cost function,
     * starting at provided initial guess. User may set other values with methods.
     *
     * @param fun The cost function to be optimised.
     * @param x0  Initial guess: a point, where iteration starts.
     */
    public AcmConjugateGradientMethod(CostFunction fun, double[] x0) {
        this.fun = fun;
        this.initialGuess = x0;
        this.maxIter = 100;
        this.maxEval = Integer.MAX_VALUE; // no limit for the number of function evaluations
        this.goalType = GoalType.MINIMIZE;
        this.convergenceRelativeTolerance = 1e-8;
        this.convergenceAbsoluteTolerance = 1e-8;
        this.formula = NonLinearConjugateGradientOptimizer.Formula.POLAK_RIBIERE;
        this.lineSearchRelativeTolerance = 1e-8;
        this.lineSearchAbsoluteTolerance = 1e-8;
        this.lineSearchBracketingRange = 1.0;
        this.preconditioner = new NonLinearConjugateGradientOptimizer.IdentityPreconditioner();
    }

    /**
     * Instantiates conjugate gradient method with default values for optimisation of provided cost function.
     * User may set other values with methods.
     *
     * @param fun The cost function to be optimised.
     */
    public AcmConjugateGradientMethod(CostFunction fun) {
        this(fun, new double[fun.dimensionality()]);
    }

    /**
     * Sets the cost function which is to be optimised.
     * @param fun The cost function which is to be optimised.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod costFunction(CostFunction fun) {
        this.fun = fun;
        return this;
    }

    /**
     * Sets the initial guess: a point, where iteration starts.
     *
     * @param x0 A point, where iteration starts.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod initialGuess(double[] x0) {
        if (x0.length != fun.dimensionality()) { throw new IllegalArgumentException(
                "Dimensionality of provided point does not match the dimensionality of the domain of cost function.");}
        this.initialGuess = x0;
        return this;
    }

    /**
     * Sets the maximum number of method iterations.
     *
     * @param m The maximum number of method iterations.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod maxIter(int m) {
        this.maxIter = m;
        return this;
    }

    /**
     * Sets the maximum number of function evaluations.
     *
     * @param m The maximum number of function evaluations.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod maxEval(int m) {
        this.maxEval = m;
        return this;
    }

    /**
     * Sets the goal type of optimisation: maximization or minimization
     *
     * @param goalType The type of optimisation: maximization or minimization.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod goalType(GoalType goalType) {
        this.goalType = goalType;
        return this;
    }

    /**
     * Sets the relative tolerance in convergence checker.
     *
     * @param eps The relative tolerance in convergence checker.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod relativeTolerance(double eps) {
        this.convergenceRelativeTolerance = eps;
        return this;
    }

    /**
     * Sets the absolute tolerance in convergence checker.
     *
     * @param eps The absolute tolerance in convergence checker.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod absoluteTolerance(double eps) {
        this.convergenceAbsoluteTolerance = eps;
        return this;
    }

    /**
     * Sets the update formula for conjugate search direction: Polak-Ribiere or Fletcher-Reeves.
     *
     * @param formula The update formula for conjugate search direction: Polak-Ribiere or Fletcher-Reeves.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod formula(NonLinearConjugateGradientOptimizer.Formula formula) {
        this.formula = formula;
        return this;
    }

    /**
     * Sets the parameters for line search.
     * @param relativeTolerance Relative tolerance used in line search.
     * @param absoluteTolerance Absolute tolerance used in line search.
     * @param bracketingRange Initial bracketing range used in line search.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod lineSearchParameters(double relativeTolerance, double absoluteTolerance,
                                                           double bracketingRange) {
        this.lineSearchRelativeTolerance = relativeTolerance;
        this.lineSearchAbsoluteTolerance = absoluteTolerance;
        this.lineSearchBracketingRange = bracketingRange;
        return this;
    }

    /**
     * Sets the preconditioner for cost function.
     *
     * @param p The preconditioner for cost function.
     * @return This instance itself.
     */
    public AcmConjugateGradientMethod preconditioner(Preconditioner p) {
        this.preconditioner = p;
        return this;
    }

    public double[] optimise() {
        // function to be optimised
        ObjectiveFunction f = new ObjectiveFunction(x -> fun.getValueAt(x));
        // gradient of function
        ObjectiveFunctionGradient df = new ObjectiveFunctionGradient(x -> fun.getValueAndGradientAt(x).gradient);
        // convergence checker
        SimplePointChecker<PointValuePair> convergenceChecker = new SimplePointChecker<>(
                convergenceRelativeTolerance, convergenceAbsoluteTolerance, maxIter);
        // instance for non-linear conjugate gradient optimization
        NonLinearConjugateGradientOptimizer nlOpt = new NonLinearConjugateGradientOptimizer(
                formula,
                convergenceChecker,
                lineSearchRelativeTolerance,
                lineSearchAbsoluteTolerance,
                lineSearchBracketingRange,
                preconditioner);
        // start optimization
        PointValuePair pvp = nlOpt.optimize(f, df,
                new InitialGuess(initialGuess),
                new MaxIter(maxIter),
                new MaxEval(maxEval),
                goalType);
        return pvp.getPoint();
    }


}
