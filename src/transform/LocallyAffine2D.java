package transform;

import gridfun.Array2D;
import util.Point2D;

import java.util.function.DoubleUnaryOperator;

/**
 * Locally affine transformation in 2D.
 *
 * This class stores the information needed to compute the transformation
 *     F: ℝ² -> ℝ², F(u) = V(r)*R(r)*(u - u₀) + u₀
 * and its Jacobi matrix or gradient with respect to transformation parameters, if needed.
 * Here, u₀ = (x₀, y₀) ∈ ℝ² is the center of transformation,
 *     r = ‖u - u₀‖ = √((x - x₀)² + (y - y₀)²) and
 *     V, R: (0,∞) -> ℝ^(2⨯2)
 * are matrix-valued functions, constructed as described below.
 * The influence range of transformation is given by continuously differentiable
 * function λ : (0,∞) -> [0, 1] and scale parameter a > 0, which shows itself in λ(a*r).
 * At given r > 0, matrix V(r) is a convex combination of Vg and I,
 *     V(r) = λ(a*r)*Vg + (1 - λ(a*r))*I,
 * where I is 2⨯2 identity matrix and
 *     Vg = s₁*v₁*v₁ᵀ + s₂*v₂*v₂ᵀ,
 * is a symmetric positive definite matrix, given by
 *     (1) orthonormal basis {v₁, v₂} for ℝ², which is standard basis {e₁, e₂}
 *         rotated around origin by angle ϑ and
 *     (2) principal stretches s₁, s₂ > 0 along principal directions v₁ and v₂.
 * V(r) is equivalent to its expanded form
 *     V(r) = λ(a*r)*((s₁ - s₂)*v₁*v₁ᵀ + (s₂ - 1)*I) + I,
 * where matrix representation of v₁*v₁ᵀ in standard basis is
 *     ⎡   cos²(ϑ)     cos(ϑ)*sin(ϑ)⎤
 *     ⎣cos(ϑ)*sin(ϑ)     sin²(ϑ)   ⎦.
 * For the purpose of efficient evaluation of values of transformation V(r), this class
 * stores elements {ξᵢⱼ | i, j = 1, 2} of matrix  Ξ = (s₁ - s₂)*v₁*v₁ᵀ + (s₂ - 1)*I, which
 * is parametrized with ϑ, s₁ and s₂ only. The whole V(r) is additionally parametrized with
 * a, x₀ and y₀ through factor λ(a*r).
 * Transformation R(r) at given r is a rotation by angle λ(a*r)*φ and its matrix
 * representation is
 *     ⎡cos(λ(a*r)*φ)  -sin(λ(a*r)*φ)⎤
 *     ⎣sin(λ(a*r)*φ)   cos(λ(a*r)*φ)⎦.
 * R(r) is parametrized with angle φ and with parameters a, x₀, y₀ through term λ(a*r).
 * The whole transformation F = F(u;θ) is parametrized with parameter vector
 *     θ = [x₀, y₀, ϑ, s₁, s₂, φ, a] from open subset of ℝ⁷.
 */
public class LocallyAffine2D implements Transformation2D {

    /*
    * This class contains variables and methods, used to calculate the value ∂F(u;θ), Jacobi matrix ∂F(u;θ)/∂u
    * and gradient ∂F(u;θ)/∂θ of the transformation F efficiently. These variables are stored in several sets
    * for clarification.
    * */

    /*
    * SET 0:
    * Variables, independent of the input point u; they depend on the transformation parameters θ only,
    * therefore they are computed only when the transformation parameters change.
    * */

    private double x0; // x-coordinate of the center of the transformation
    private double y0; // y-coordinate of the center of the transformation
    private double phi; // φ, rotation for R
    private double a; // scale parameter
    private final DoubleUnaryOperator lamFun; // function λ : (0,∞) -> [0, 1]
    private final DoubleUnaryOperator lamDerFun; // derivative of function λ, λ'(x) = dλ(x)/dx

    // Elements of symmetric matrix Ξ = (s₁ - s₂)*v₁*v₁ᵀ + (s₂ - 1)*I = Vg - I,
    // stored for efficient evaluation of V(r) = λ(a*r)*Ξ + I:
    private double xi_11;
    private double xi_12;
    private double xi_22;

    // Elements of symmetric matrices
    // P₁ = v₁*v₁ᵀ = ⎡   cos²(ϑ)     cos(ϑ)*sin(ϑ)⎤
    //               ⎣cos(ϑ)*sin(ϑ)     sin²(ϑ)   ⎦  and
    // P₂ = v₂*v₂ᵀ = ⎡   sin²(ϑ)     -cos(ϑ)*sin(ϑ)⎤
    //               ⎣-cos(ϑ)*sin(ϑ)     cos²(ϑ)   ⎦,
    // which appear in ∂V/∂sᵢ = λ(a*r)*Pᵢ, (i = 1, 2) :
    private double cosSqTheta; // cos²(ϑ)
    private double sinSqTheta; // sin²(ϑ)
    private double cosSinTheta; // cos(ϑ)*sin(ϑ)

    // Elements of symmetric matrix H, which appears in ∂V/∂ϑ = λ(a*r)*H,
    //     H = (s₁ - s₂)*⎡-2*cos(ϑ)*sin(ϑ)   cos²(ϑ) - sin²(ϑ)⎤
    //                   ⎣cos²(ϑ) - sin²(ϑ)   2*cos(ϑ)*sin(ϑ) ⎦ :
    private double h_22; // 2*cos(ϑ)*sin(ϑ)
    private double h_12; // cos²(ϑ) - sin²(ϑ)

    /**
     * Constructs locally affine transformation in 2D with default parameters and given lambda function
     * and its derivative.
     * @param lambdaFun Function λ.
     * @param lambdaDerivativeFun Function λ', derivative of λ.
     */
    public LocallyAffine2D(DoubleUnaryOperator lambdaFun, DoubleUnaryOperator lambdaDerivativeFun) {
        this.x0 = 0;
        this.y0 = 0;
        this.phi = 0;
        this.a = 1;
        this.lamFun = lambdaFun;
        this.lamDerFun = lambdaDerivativeFun;
        this.xi_11 = 0;
        this.xi_12 = 0;
        this.xi_22 = 0;
        this.cosSqTheta = 1;
        this.sinSqTheta = 0;
        this.cosSinTheta = 0;
        this.h_22 = 0;
        this.h_12 = 1;
    }

    /**
     * Changes the parameters of this transformation.
     * @param theta New parameters for this transformation, θ = [x₀, y₀, ϑ, s₁, s₂, φ, a].
     */
    @Override
    public void changeParameters(double[] theta) {
        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        this.x0 = theta[0];
        this.y0 = theta[1];
        this.phi = theta[5];
        this.a = theta[6];

        double c = Math.cos(theta[2]);
        this.cosSqTheta = c * c;
        this.sinSqTheta = 1 - this.cosSqTheta;
        this.cosSinTheta = cosSqTheta * sinSqTheta;

        this.h_22 = 2 * this.cosSinTheta;
        this.h_12 = this.cosSqTheta - this.sinSqTheta;

        // Ξ = (s₁ - s₂)*P₁ + (s₂ - 1)*I
        double sdif = theta[3] - theta[4]; // s₁ - s₂
        double sme = theta[4] - 1; // s₂ - 1
        this.xi_11 = sdif * cosSqTheta + sme;
        this.xi_12 = sdif * cosSinTheta;
        this.xi_22 = sdif * sinSqTheta + sme;
    }

    /**
     * Applies this transformation to a given input point and returns the resulting point.
     * @param u A point, at which this transformation should be evaluated.
     * @return Value of this transformation at a given input point.
     */
    @Override
    public Point2D map(Point2D u) {
        double d_x = u.x - x0; //  d = [d_x, d_y]ᵀ := [x - x₀, y - y₀]ᵀ = u - u₀
        double d_y = u.y - y0;
        double r = Math.sqrt(d_x * d_x + d_y * d_y); // norm of u - u₀
        double ar = a * r;
        double lam = lamFun.applyAsDouble(ar); // λ(a*r)

        // elements of ez-matrix R(r)
        //      ⎡cos(λ(a*r)*φ)  -sin(λ(a*r)*φ)⎤
        //      ⎣sin(λ(a*r)*φ)   cos(λ(a*r)*φ)⎦
        double cosLPhi = Math.cos(lam * phi); // cos(λ(a*r)*φ)
        double sinLPhi = Math.sqrt(1 - cosLPhi * cosLPhi); // sin(λ(a*r)*φ)

        // elements of symmetric matrix V(r) = λ(a*r)*Ξ + I
        //     V(r) = λ(a*r)*⎡ξ₁₁ ξ₁₂⎤ + ⎡1 0⎤
        //                   ⎣ξ₁₂ ξ₂₂⎦   ⎣0 1⎦
        double v_11 = lam * xi_11 + 1;
        double v_12 = lam * xi_12;
        double v_22 = lam * xi_22 + 1;

        // product R(r)*d
        double rd_x = cosLPhi * d_x - sinLPhi * d_y;
        double rd_y = sinLPhi * d_x + cosLPhi * d_y;

        // value of transformation, V(r)*R(r)*d + u₀
        return new Point2D(v_11 * rd_x + v_12 * rd_y + x0, v_12 * rd_x + v_22 * rd_y + y0);
    }

    /*
     * f = [f_1, f_2], then Jf_(x, y) = [∂f_1/∂x, ∂f_1/∂y; ∂f_2/∂x, ∂f_2/∂y].
     * */

    /**
     * Applies this transformation to a given input point and returns the resulting point together with
     * gradient ∂F(u;θ)/∂θ or Jacobi matrix ∂F(u;θ)/∂u, if specified.
     * @param u A point, at which this transformation and its derivatives should be evaluated.
     * @param calculateGradient Indicates whether the gradient ∂F(u;θ)/∂θ should be calculated.
     * @param calculateJacobi Indicates whether the Jacobi matrix ∂F(u;θ)/∂u should be calculated.
     * @return Result of this transformation at given input point, containing the value and derivatives.
     * If {@code calculateGradient} or {@code calculateJacobi} is false, then respective field in
     * {@code Transformation2D.Result} will be null.
     */
    public Transformation2D.Result map(Point2D u, boolean calculateGradient, boolean calculateJacobi) {
        /*
        * SET 1:
        * Variables, needed to compute any one of the three desired results.
        * */

        double d_x = u.x - x0; //  d = [d_x, d_y]ᵀ := [x - x₀, y - y₀]ᵀ = u - u₀
        double d_y = u.y - y0;
        double r = Math.sqrt(d_x * d_x + d_y * d_y); // norm of u - u₀
        double ar = a * r;
        double lam = lamFun.applyAsDouble(ar); // λ(a*r)

        // elements of ez-matrix R(r)
        //      ⎡cos(λ(a*r)*φ)  -sin(λ(a*r)*φ)⎤
        //      ⎣sin(λ(a*r)*φ)   cos(λ(a*r)*φ)⎦
        double cosLPhi = Math.cos(lam * phi); // cos(λ(a*r)*φ)
        double sinLPhi = Math.sqrt(1 - cosLPhi * cosLPhi); // sin(λ(a*r)*φ)

        // elements of symmetric matrix V(r) = λ(a*r)*Ξ + I
        //     V(r) = λ(a*r)*⎡ξ₁₁ ξ₁₂⎤ + ⎡1 0⎤
        //                   ⎣ξ₁₂ ξ₂₂⎦   ⎣0 1⎦
        double v_11 = lam * xi_11 + 1;
        double v_12 = lam * xi_12;
        double v_22 = lam * xi_22 + 1;

        // product R(r)*d
        double rd_x = cosLPhi * d_x - sinLPhi * d_y;
        double rd_y = sinLPhi * d_x + cosLPhi * d_y;

        // value of transformation, V(r)*R(r)*d + u₀
        Point2D value = new Point2D(v_11 * rd_x + v_12 * rd_y + x0, v_12 * rd_x + v_22 * rd_y + y0);

        // We assume henceforth that either calculateGradient or calculateJacobi is true, otherwise user would call
        // method map(Point2D u) instead.

        /*
        * SET 2:
        * Variables, needed to compute either the Jacobi matrix or the gradient.
        * */

        // product Ξ*R(r)*d
        double xird_x = xi_11 * rd_x + xi_12 * rd_y;
        double xird_y = xi_12 * rd_x + xi_22 * rd_y;

        // product V(r)*B(r)   B = [-1 0; 0 1]*flipud(R(r))
        double tauc = v_12 * sinLPhi;
        double taus = v_12 * cosLPhi;
        double vb_11 = -v_11 * sinLPhi + tauc;
        double vb_12 = -v_11 * cosLPhi - taus;
        double vb_21 = v_22 * cosLPhi - taus;
        double vb_22 = -v_22 * sinLPhi - tauc;

        // product V(r)*B(r)*d
        double vbd_x = vb_11 * d_x + vb_12 * d_y;
        double vbd_y = vb_21 * d_x + vb_22 * d_y;

        // z = {Ξ*R(r) + φ*V(r)*B(r)}*d
        double z_x = xird_x + phi * vbd_x;
        double z_y = xird_y + phi * vbd_y;

        double lamDer = lamDerFun.applyAsDouble(ar);

        double j = a * lamDer / r;
        double j_x = j * d_x;
        double j_y = j * d_y;
        // W = j*z*dᵀ + V(r)*R(r)
        double w_11 = j_x * z_x - vb_12;
        double w_12 = j_y * z_x + vb_11;
        double w_21 = j_x * z_y - vb_22;
        double w_22 = j_y * z_y + vb_21;

        Array2D jacobi = calculateJacobi ? new Array2D(2, 2, new double[]{w_11, w_12, w_21, w_22}) : null;

        if (calculateGradient) {

            Array2D gradient = new Array2D(2, 7);

            // [∂F/∂x₀, ∂F/∂y₀] = - W + I
            gradient.values[0] = -w_11 + 1; // ∂F/∂x₀, x-component
            gradient.values[7] = -w_21; // ∂F/∂x₀, y-component
            gradient.values[1] = -w_12; // ∂F/∂y₀, x-component
            gradient.values[8] = -w_22 + 1; // ∂F/∂y₀, y-component

            // ∂F/∂ϑ = λ(a*r)*H*R(r)*d
            double lrd_x = lam * rd_x; // λ(a*r)*R(r)*d
            double lrd_y = lam * rd_y;
            gradient.values[2] = -h_22 * lrd_x + h_12 * lrd_y;
            gradient.values[9] = h_12 * lrd_x + h_22 * lrd_y;

            // ∂F/∂s₁ = λ(a*r)*P₁*R(r)*d
            double csx = cosSinTheta * lrd_x;
            double csy = cosSinTheta * lrd_y;
            gradient.values[3] = cosSqTheta * lrd_x + csy;
            gradient.values[10] = csx + sinSqTheta * lrd_y;

            // ∂F/∂s₂ = λ(a*r)*P₂*R(r)*d
            gradient.values[4] = sinSqTheta * lrd_x - csy;
            gradient.values[11] = -csx + cosSqTheta * lrd_y;

            // ∂F/∂φ = λ(a*r)*V(r)*B(r)*d
            gradient.values[5] = lam * vbd_x;
            gradient.values[12] = lam * vbd_y;

            // ∂F/∂a = λ'(a*r)*r*z
            double k = lamDer * r;
            gradient.values[6] = k * z_x;
            gradient.values[13] = k * z_y;

            return new Transformation2D.Result(value, gradient, jacobi);

        } else {
            return new Transformation2D.Result(value, null, jacobi);
        }
    }

    /**
     * Returns the number of parameters by which this transformation is parametrised.
     * @return 7 (always), because that is the number of parameters (dimensionality of parameter space).
     */
    @Override
    public int numberOfParameters() {
        return 7;
    }

}
