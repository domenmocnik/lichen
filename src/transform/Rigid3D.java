package transform;

import gridfun.Array2D;
import util.Point3D;

/**
 * Rigid transformation in 3-dimensional Euclidean space, T: ℝ³ -> ℝ ³, T(u) = R(u - c) + c + t, where R is the
 * orthonormal matrix, representing rotation, c is the center of rotation and t is the translation vector.
 *
 * Parameters of transformation [theta, phi, psi, t_x, t_y, t_z] are:
 * theta and phi, spherical coordinates of unit vector w of rotation,
 * psi, an angle of rotation around rotation vector w,
 * t_x, t_y and t_z, Cartesian components of translation vector t.
 * The center of rotation c must be provided by user.
 */
public class Rigid3D implements Transformation3D {

    private boolean derivatives; // indicates whether this object will be used to calculate derivatives

    private double c1; // coordinates of the center of rotation
    private double c2;
    private double c3;
    private double totalShift1; // coordinates of the total translation
    private double totalShift2;
    private double totalShift3;

    private double matR_11 = 1; // elements of rotation matrix R
    private double matR_12 = 0;
    private double matR_13 = 0;
    private double matR_21 = 0;
    private double matR_22 = 1;
    private double matR_23 = 0;
    private double matR_31 = 0;
    private double matR_32 = 0;
    private double matR_33 = 1;

    private double matRW_11 = 0; // elements of product matrix RW
    private double matRW_12 = -1;
    private double matRW_13 = 0;
    private double matRW_21 = 1;
    private double matRW_22 = 0;
    private double matRW_23 = 0;
    private double matRW_31 = 0;
    private double matRW_32 = 0;
    private double matRW_33 = 0;

    private double matDtR_11 = 0;
    private double matDtR_12 = 0;
    private double matDtR_13 = 0;
    private double matDtR_21 = 0;
    private double matDtR_22 = 0;
    private double matDtR_23 = 0;
    private double matDtR_31 = 0;
    private double matDtR_32 = 0;
    private double matDtR_33 = 0;

    private double matDfR_11 = 0;
    private double matDfR_12 = 0;
    private double matDfR_13 = 0;
    private double matDfR_21 = 0;
    private double matDfR_22 = 0;
    private double matDfR_23 = 0;
    private double matDfR_31 = 0;
    private double matDfR_32 = 0;
    private double matDfR_33 = 0;


    /**
     * Constructs rigid transformation with given center and default parameters.
     *
     * @param center Center of rotation.
     * @param derivatives Indicates whether this object will be also used to calculate derivatives.
     */
    public Rigid3D(Point3D center, boolean derivatives) {
        this.derivatives = derivatives;
        this.c1 = center.x;
        this.c2 = center.y;
        this.c3 = center.z;
        this.totalShift1 = center.x;
        this.totalShift2 = center.y;
        this.totalShift3 = center.z;
    }

    /**
     * Constructs rigid transformation with given center and default parameters.
     *
     * @param center Center of rotation.
     */
    public Rigid3D(Point3D center) {
        this(center, true);
    }

    /**
     * Indicates whether this object should prepare its internal state for calculation of derivatives each time
     * the method changeParameters() is called.
     *
     * @param b True if this object will be also used to calculate derivatives, false otherwise.
     */
    public void calculateDerivatives(boolean b) {
        this.derivatives = b;
    }

    /**
     * Changes the center of rotation.
     *
     * @param center New center of rotation.
     */
    public void changeCenterOfRotation(Point3D center) {
        double t1 = this.totalShift1 - this.c1;
        double t2 = this.totalShift2 - this.c2;
        double t3 = this.totalShift3 - this.c3;
        this.c1 = center.x;
        this.c2 = center.y;
        this.c3 = center.z;
        this.totalShift1 = center.x + t1;
        this.totalShift2 = center.y + t2;
        this.totalShift3 = center.z + t3;
    }

    /**
     * Returns the result of applying this transformation to a given point.
     *
     * @param u A point, at which this transformation should be evaluated.
     * @return The result of applying this transformation to a given input point.
     */
    @Override
    public Point3D map(Point3D u) {
        double r1 = u.x - c1;
        double r2 = u.y - c2;
        double r3 = u.z - c3;
        double x = matR_11 * r1 + matR_12 * r2 + matR_13 * r3 + totalShift1;
        double y = matR_21 * r1 + matR_22 * r2 + matR_23 * r3 + totalShift2;
        double z = matR_31 * r1 + matR_32 * r2 + matR_33 * r3 + totalShift3;
        return new Point3D(x, y, z);
    }

    /**
     * Applies the transformation to a given input point and returns the resulting point together with
     * gradient ∂T(u;θ)/∂θ or Jacobi matrix ∂T(u;θ)/∂u, if specified.
     *
     * @param u                 A point, at which the transformation and its derivatives should be evaluated.
     * @param calculateGradient Indicates whether the gradient ∂T(u;θ)/∂θ should be calculated.
     * @param calculateJacobi   Indicates whether the Jacobi matrix ∂T(u;θ)/∂u should be calculated.
     * @return Result of this transformation at given input point, containing the value and derivatives.
     * If {@code calculateGradient} or {@code calculateJacobi} is false, then respective field in
     * {@code Transformation3D.Result} will be null.
     */
    @Override
    public Result map(Point3D u, boolean calculateGradient, boolean calculateJacobi) {
        double r1 = u.x - c1;
        double r2 = u.y - c2;
        double r3 = u.z - c3;
        double x = matR_11 * r1 + matR_12 * r2 + matR_13 * r3 + totalShift1;
        double y = matR_21 * r1 + matR_22 * r2 + matR_23 * r3 + totalShift2;
        double z = matR_31 * r1 + matR_32 * r2 + matR_33 * r3 + totalShift3;
        Point3D value = new Point3D(x, y, z);

        Array2D gradient = null;
        if (calculateGradient) {
            // parameters: [theta, phi, psi, t_x, t_y, t_z]
            gradient = new Array2D(3, 6);
            // d T / d theta
            gradient.values[0] = matDtR_11 * r1 + matDtR_12 * r2 + matDtR_13 * r3;
            gradient.values[6] = matDtR_21 * r1 + matDtR_22 * r2 + matDtR_23 * r3;
            gradient.values[12] = matDtR_31 * r1 + matDtR_32 * r2 + matDtR_33 * r3;
            // d T / d phi
            gradient.values[1] = matDfR_11 * r1 + matDfR_12 * r2 + matDfR_13 * r3;
            gradient.values[7] = matDfR_21 * r1 + matDfR_22 * r2 + matDfR_23 * r3;
            gradient.values[13] = matDfR_31 * r1 + matDfR_32 * r2 + matDfR_33 * r3;
            // d T / d psi
            gradient.values[2] = matRW_11 * r1 + matRW_12 * r2 + matRW_13 * r3;
            gradient.values[8] = matRW_21 * r1 + matRW_22 * r2 + matRW_23 * r3;
            gradient.values[14] = matRW_31 * r1 + matRW_32 * r2 + matRW_33 * r3;
            // d T / d t_.
            gradient.values[3] = 1; gradient.values[4] = 0; gradient.values[5] = 0;
            gradient.values[9] = 0; gradient.values[10] = 1; gradient.values[11] = 0;
            gradient.values[15] = 0; gradient.values[16] = 0; gradient.values[17] = 1;
        }

        Array2D jacobi = null;
        if (calculateJacobi) {
            jacobi = new Array2D(3, 3);
            jacobi.values[0] = matR_11; jacobi.values[1] = matR_12; jacobi.values[2] = matR_13;
            jacobi.values[3] = matR_21; jacobi.values[4] = matR_22; jacobi.values[5] = matR_23;
            jacobi.values[6] = matR_31; jacobi.values[7] = matR_32; jacobi.values[8] = matR_33;
        }

        return new Transformation3D.Result(value, gradient, jacobi);
    }

    /**
     * Changes the parameters of this transformation.
     *
     * @param theta New parameters for this transformation.
     */
    @Override
    public void changeParameters(double[] theta) {
        // parameters: [theta, phi, psi, t_x, t_y, t_z]

        totalShift1 = c1 + theta[3];
        totalShift2 = c2 + theta[4];
        totalShift3 = c3 + theta[5];

        double cosPsi = Math.cos(theta[2]); // cos(ψ)
        double k = 1 - cosPsi;
        double sinPsi = Math.sqrt(1 - cosPsi * cosPsi);

        double cosTheta = Math.cos(theta[0]);
        double sinTheta = Math.sqrt(1 - cosTheta * cosTheta);
        double w3 = Math.cos(theta[1]); // = cos(phi)
        double sinPhi = Math.sqrt(1 - w3 * w3);
        double w1 = cosTheta * sinPhi;
        double w2 = sinTheta * sinPhi;

        // symmetric matrix (1 - cos(ψ)) * w ⊗ w
        double kww_11 = k * w1 * w1;
        double kww_12 = k * w1 * w2;
        double kww_13 = k * w1 * w3;
        double kww_22 = k * w2 * w2;
        double kww_23 = k * w2 * w3;
        double kww_33 = k * w3 * w3;

        double spw1 = sinPsi * w1;
        double spw2 = sinPsi * w2;
        double spw3 = sinPsi * w3;

        matR_11 = cosPsi + kww_11;
        matR_12 = kww_12 - spw3;
        matR_13 = kww_13 + spw2;
        matR_21 = kww_12 + spw3;
        matR_22 = cosPsi + kww_22;
        matR_23 = kww_23 - spw1;
        matR_31 = kww_13 - spw2;
        matR_32 = kww_23 + spw1;
        matR_33 = cosPsi + kww_33;

        if (derivatives) {
            matRW_11 = matR_12 * w3 - matR_13 * w2;
            matRW_12 = -matR_11 * w3 + matR_13 * w1;
            matRW_13 = matR_11 * w2 - matR_12 * w1;
            matRW_21 = matR_22 * w3 - matR_23 * w2;
            matRW_22 = -matR_21 * w3 + matR_23 * w1;
            matRW_23 = matR_21 * w2 - matR_22 * w1;
            matRW_31 = matR_32 * w3 - matR_33 * w2;
            matRW_32 = -matR_31 * w3 + matR_33 * w1;
            matRW_33 = matR_31 * w2 - matR_32 * w1;

            double psi = theta[2];
            double psiR_11 =  psi * matR_11;
            double psiR_12 =  psi * matR_12;
            double psiR_13 =  psi * matR_13;
            double psiR_21 =  psi * matR_21;
            double psiR_22 =  psi * matR_22;
            double psiR_23 =  psi * matR_23;
            double psiR_31 =  psi * matR_31;
            double psiR_32 =  psi * matR_32;
            double psiR_33 =  psi * matR_33;

            matDtR_11 = -psiR_13 * w1;
            matDtR_12 = -psiR_13 * w2;
            matDtR_13 = psiR_11 * w1 + psiR_12 * w2;
            matDtR_21 = -psiR_23 * w1;
            matDtR_22 = -psiR_23 * w2;
            matDtR_23 = psiR_21 * w1 + psiR_22 * w2;
            matDtR_31 = -psiR_23 * w1;
            matDtR_32 = -psiR_23 * w2;
            matDtR_33 = psiR_21 * w1 + psiR_22 * w2;

            double mus = sinTheta * w3;
            double muc = cosTheta * w3;
            matDfR_11 = -psiR_12 * sinPhi - psiR_13 * mus;
            matDfR_12 = -psiR_11 * sinPhi + psiR_13 * muc;
            matDfR_13 = psiR_11 * mus - psiR_12 * muc;
            matDfR_21 = -psiR_22 * sinPhi - psiR_23 * mus;
            matDfR_22 = -psiR_21 * sinPhi + psiR_23 * muc;
            matDfR_23 = psiR_21 * mus - psiR_22 * muc;
            matDfR_31 = -psiR_32 * sinPhi - psiR_33 * mus;
            matDfR_32 = -psiR_31 * sinPhi + psiR_33 * muc;
            matDfR_33 = psiR_31 * mus - psiR_32 * muc;
        }
    }

    /**
     * Returns number of parameters by which this transformation is parametrised. Should always return the same
     * positive integer.
     *
     * @return Number of parameters (dimensionality of parameter space).
     */
    @Override
    public int numberOfParameters() {
        return 6;
    }
}
