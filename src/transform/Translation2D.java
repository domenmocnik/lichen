package transform;

import gridfun.Array2D;
import util.Point2D;

/**
 * Translation in 2D.
 */
public class Translation2D implements Transformation2D {

    private double shiftX;
    private double shiftY;

    public Translation2D() {
        this.shiftX = 0;
        this.shiftY = 0;
    }

    public Translation2D(double shiftX, double shiftY) {
        this.shiftX = shiftX;
        this.shiftY = shiftY;
    }

    @Override
    public Point2D map(Point2D u) {
        return new Point2D(u.x + shiftX, u.y + shiftY);
    }

    @Override
    public Transformation2D.Result map(Point2D u, boolean calculateGradient, boolean calculateJacobi) {
        return new Transformation2D.Result(
                new Point2D(u.x + shiftX, u.y + shiftY),
                calculateGradient ? new Array2D(2, 2, new double[]{1, 0, 0, 1}) : null,
                calculateJacobi ? new Array2D(2, 2,new double[]{1, 0, 0, 1}) : null);
    }

    @Override
    public void changeParameters(double[] theta) {
        shiftX = theta[0];
        shiftY = theta[1];
    }

    @Override
    public int numberOfParameters() {
        return 2;
    }

    @Override
    public double[] getPreconditionMatrix(double lx, double ly, int m, int n) {
        return new double[] { 1, 1 };
    }
}
