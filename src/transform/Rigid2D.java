package transform;

import gridfun.Array2D;
import util.Point2D;

/**
 * Rigid transformation in 2D. Rotates point u by an angle phi around center point c, followed by translation by a
 * vector t: T(u) = R(u - c) + c + t, where R is the orthonormal matrix [cos(phi), -sin(phi); sin(phi), cos(phi)].
 * The transformation vector theta is [phi, t_x, t_y]. Phi is in radians, t_x and t_y are Cartesian components of t.
 */
public class Rigid2D implements Transformation2D {

    private double cx; // x-coordinate of the center of rotation
    private double cy; // y-coordinate of the center of rotation
    // phi is the angle of rotation (in radians) around the center point
    private double cosPhi; // cos(phi)
    private double sinPhi; // sin(phi)
    private double totalShiftX; // total translation in x-direction, t_x + c_x
    private double totalShiftY; // total translation in y-direction, t_y + c_y

    public Rigid2D(double centerX, double centerY, double phi, double shiftX, double shiftY) {
        this.cx = centerX;
        this.cy = centerY;
        this.cosPhi = Math.cos(phi);
        this.sinPhi = Math.sin(phi);
        this.totalShiftX = shiftX + centerX;
        this.totalShiftY = shiftY + centerY;
    }

    public Rigid2D(double centerX, double centerY) {
        this.cx = centerX;
        this.cy = centerY;
        this.cosPhi = 1;
        this.sinPhi = 0;
        this.totalShiftX = centerX;
        this.totalShiftY = centerY;
    }

    public Rigid2D(double centerX, double centerY, double phi) {
        this(centerX, centerY, phi, 0, 0);
    }

    @Override
    public Point2D map(Point2D u) {
        double rx = u.x - cx;
        double ry = u.y - cy;
        return new Point2D(cosPhi * rx - sinPhi * ry + totalShiftX, sinPhi * rx + cosPhi * ry + totalShiftY);
    }

    @Override
    public Transformation2D.Result map(Point2D u, boolean calculateGradient, boolean calculateJacobi) {
        double rx = u.x - cx;
        double ry = u.y - cy;
        double xsin = sinPhi * rx;
        double xcos = cosPhi * rx;
        double ysin = sinPhi * ry;
        double ycos = cosPhi * ry;
        return new Transformation2D.Result(
                new Point2D(xcos - ysin + totalShiftX, xsin + ycos + totalShiftY),
                calculateGradient ?
                        new Array2D(2, 3, new double[]{-xsin - ycos, 1, 0, xcos - ysin, 0, 1}) : null,
                calculateJacobi ?
                        new Array2D(2, 2, new double[]{cosPhi, -sinPhi, sinPhi, cosPhi}) : null);
    }

    @Override
    public void changeParameters(double[] theta) {
        this.cosPhi = Math.cos(theta[0]);
        this.sinPhi = Math.sin(theta[0]);
        this.totalShiftX = theta[1] + cx;
        this.totalShiftY = theta[2] + cy;
    }

    @Override
    public int numberOfParameters() {
        return 3;
    }

    @Override
    public double[] getPreconditionMatrix(double lx, double ly, int m, int n) {
        // The precondition matrix for rigid transformation can be calculated analytically and is independent of
        // the transformation vector. Therefore this method is overridden to avoid expensive calculation.
        return new double[]{ 6 / (lx * lx + ly * ly), 1, 1 };
    }

}
