package transform;

import gridfun.Array2D;
import util.Point3D;

import java.util.function.DoubleUnaryOperator;

/**
 * Locally similarity transformation in 3D. Parameterised by parameter vector θ = [c1, c2, c3, s, ϑ, φ, ψ, a]^T.
 */
public class LocallySimilarityTransform3D implements Transformation3D {

    private double c1 = 0; // components of center vector c = [c1, c2, c3]^T
    private double c2 = 0;
    private double c3 = 0;
    private double sme = 0; // s - 1, s is scale parameter in matrix V
    private double a = 1; // region influence parameter
    private double w1 = 0; // components of rotation vector w = [w1, w2, w3]^T
    private double w2 = 0;
    private double w3 = 1;
    private double psi = 0; // angle ψ of local rotation

    private double ww_11 = 0; // elements of symmetric matrix w ⊗ w, (w ⊗ w)_ij = w_i * w_j
    private double ww_12 = 0;
    private double ww_13 = 0;
    private double ww_22 = 0;
    private double ww_23 = 0;
    private double ww_33 = 1;

    // NOTE: next three fields are only needed in computation of derivatives (only for gradient)
    private double sinPhi = 1; // sin(φ)
    private double sinThetaCosPhi = 0; // sin(ϑ) * cos(φ)
    private double cosThetaCosPhi = 0; // cos(ϑ) * cos(φ)

    private final DoubleUnaryOperator lamFun; // function λ : (0,∞) -> [0, 1]
    private final DoubleUnaryOperator lamDerFun; /* λ', the derivative of λ. NOTE: needed only for computation of
       gradient and Jacobi Matrix. */


    /**
     * Constructs a locally similarity transformation in 3D with default parameters and given lambda function
     * and its derivative.
     *
     * If derivative of lambda is not provided (is null), then an invocation of method map(Point3D, boolean, boolean)
     * on constructed instance will throw an UnsupportedOperationException. The function lambda should never be null.
     *
     * @param lamFun Function λ that controls the influence of similarity transform.
     * @param lamDerFun λ', the derivative of λ. Can be null, but then the calculation of derivatives of transformation
     *                  is not supported.
     */
    public LocallySimilarityTransform3D(DoubleUnaryOperator lamFun, DoubleUnaryOperator lamDerFun) {
        if (lamFun == null) {
            throw new IllegalArgumentException("Provided lambda function should not be null.");
        }
        this.lamFun = lamFun;
        this.lamDerFun = lamDerFun;
    }

    /**
     * Returns the result of applying this transformation to a given point.
     *
     * @param u A point, at which this transformation should be evaluated.
     * @return The result of applying this transformation to a given input point.
     */
    @Override
    public Point3D map(Point3D u) {
        double d1 = u.x - c1; // components of vector d = u - c
        double d2 = u.y - c2;
        double d3 = u.z - c3;
        double r = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3); // norm of vector d = u - c
        double ar = a * r;
        double lam = lamFun.applyAsDouble(ar);
        double v = lam * sme + 1; // λ(a * r) * (s - 1) + 1, matrix V is V = v * I

        double lap = lam * psi; // λ(a * r) * ψ
        double clap = Math.cos(lap);
        double slap = Math.sqrt(1 - clap * clap);
        double emclap = 1 - clap;

        return new Point3D(
                v * (clap * d1 + emclap * (ww_11 * d1 + ww_12 * d2 + ww_13 * d3) + slap * (-w3 * d2 + w2 * d3)) + c1,
                v * (clap * d2 + emclap * (ww_12 * d1 + ww_22 * d2 + ww_23 * d3) + slap * (w3 * d1 - w1 * d3)) + c2,
                v * (clap * d3 + emclap * (ww_13 * d1 + ww_23 * d2 + ww_33 * d3) + slap * (-w2 * d1 + w1 * d2)) + c3
        );
    }

    /**
     * Applies the transformation to a given input point and returns the resulting point together with
     * gradient ∂T(u;θ)/∂θ or Jacobi matrix ∂T(u;θ)/∂u, if specified.
     *
     * It is assumed that gradient or Jacobi are requested. If user needs only the transformed point, then he
     * should use method map(Point3D) instead as it is more efficient.
     *
     * @param u A point, at which the transformation and its derivatives should be evaluated.
     * @param calculateGradient Indicates whether the gradient ∂T(u;θ)/∂θ should be calculated.
     * @param calculateJacobi Indicates whether the Jacobi matrix ∂T(u;θ)/∂u should be calculated.
     * @return Result of this transformation at given input point, containing the value and derivatives.
     * If {@code calculateGradient} or {@code calculateJacobi} is false, then respective field in
     * {@code Transformation3D.Result} will be null.
     */
    @Override
    public Transformation3D.Result map(Point3D u, boolean calculateGradient, boolean calculateJacobi) {
        if (lamDerFun == null) {
            throw new UnsupportedOperationException("Calculation of derivatives of this transformation is not" +
                    "supported, because the derivative of function lambda was not provided in the constructor.");
        }

        // --- In this first section are values, that are needed for both jacobi matrix and gradient. ---
        double d1 = u.x - c1; // components of vector d = u - c
        double d2 = u.y - c2;
        double d3 = u.z - c3;
        double r = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3); // norm of vector d = u - c
        double ar = a * r;
        double lam = lamFun.applyAsDouble(ar);
        double lamDer = lamDerFun.applyAsDouble(ar);
        double v = lam * sme + 1; // λ(a * r) * (s - 1) + 1, matrix V is V = v * I

        double lap = lam * psi; // λ(a * r) * ψ
        double clap = Math.cos(lap); // cos(λψ)
        double slap = Math.sqrt(1 - clap * clap); // sin(λψ)
        double emclap = 1 - clap; // 1 - cos(λψ)

        // elements of matrix R
        double r_11 = clap + emclap * ww_11;
        double r_22 = clap + emclap * ww_22;
        double r_33 = clap + emclap * ww_33;
        double m = emclap * ww_12; double k = slap * w3;
        double r_12 = m - k;
        double r_21 = m + k;
        m = emclap * ww_13; k = slap * w2;
        double r_13 = m + k;
        double r_31 = m - k;
        m = emclap * ww_23; k = slap * w1;
        double r_23 = m - k;
        double r_32 = m + k;

        // elements of vector which is a product of R * d
        double rd_1 = r_11 * d1 + r_12 * d2 + r_13 * d3;
        double rd_2 = r_21 * d1 + r_22 * d2 + r_23 * d3;
        double rd_3 = r_31 * d1 + r_32 * d2 + r_33 * d3;

        // elements of vector g which is a product of ∂W/∂ϑ * d =: g
        double g1 = w1 * d3;
        double g2 = w2 * d3;
        // g3 will be computed later only if gradient is needed

        // elements of vector which is a product of W * d
        double wd_1 = -w3 * d2 + g2;
        double wd_2 = w3 * d1 - g1;
        double wd_3 = -w2 * d1 + w1 * d2;

        // elements of vector which is a product of R * W * d
        double rwd_1 = r_11 * wd_1 + r_12 * wd_2 + r_13 * wd_3;
        double rwd_2 = r_21 * wd_1 + r_22 * wd_2 + r_23 * wd_3;
        double rwd_3 = r_31 * wd_1 + r_32 * wd_2 + r_33 * wd_3;

        // these values are needed for jacobi matrix and for partials ∂T/∂c_i
        double vp = v * psi; // v * ψ
        k = lamDer * a / r;
        m = vp * k; // v * ψ * λ' * a * r⁻¹
        k = sme * k; // (s - 1) * λ' * a * r⁻¹
        double z1 = k * rd_1 + m * rwd_1;
        double z2 = k * rd_2 + m * rwd_2;
        double z3 = k * rd_3 + m * rwd_3;
        // ∂T/∂x_i = d_i * λ' * a * r⁻¹ * ((s - 1) * R * d + v * ψ * R * W * d) + v * R * e_i
        double j_11 = d1 * z1 + v * r_11;
        double j_12 = d2 * z1 + v * r_12;
        double j_13 = d3 * z1 + v * r_13;
        double j_21 = d1 * z2 + v * r_21;
        double j_22 = d2 * z2 + v * r_22;
        double j_23 = d3 * z2 + v * r_23;
        double j_31 = d1 * z3 + v * r_31;
        double j_32 = d2 * z3 + v * r_32;
        double j_33 = d3 * z3 + v * r_33;

        // --- Jacobi matrix ---
        Array2D jacobi = null;
        if (calculateJacobi) {
            jacobi = new Array2D(3, 3, new double[]{j_11, j_12, j_13, j_21, j_22, j_23, j_31, j_32, j_33});
        }

        // --- Gradient ---
        Array2D gradient = null;
        if (calculateGradient) {

            double g3 = -w1 * d1 - w2 * d2;

            // elements of vector which is a product of R * ∂W/∂ϑ * d = R * g
            double rg_1 = r_11 * g1 + r_12 * g2 + r_13 * g3;
            double rg_2 = r_21 * g1 + r_22 * g2 + r_23 * g3;
            double rg_3 = r_31 * g1 + r_32 * g2 + r_33 * g3;

            // elements of vector h which is a product of ∂W/∂φ * d =: h
            double h1 = sinPhi * d2 + sinThetaCosPhi * d3;
            double h2 = -sinPhi * d1 - cosThetaCosPhi * d3;
            double h3 = -sinThetaCosPhi * d1 + cosThetaCosPhi * d2;

            // elements of vector which is a product of R * ∂W/∂φ * d = R * h
            double rh_1 = r_11 * h1 + r_12 * h2 + r_13 * h3;
            double rh_2 = r_21 * h1 + r_22 * h2 + r_23 * h3;
            double rh_3 = r_31 * h1 + r_32 * h2 + r_33 * h3;

            gradient = new Array2D(3, 8);
            double[] grad = gradient.values;

            // partials ∂T/∂c_i = - { d_i * λ' * a * r⁻¹ * ((s - 1) * R * d + v * ψ * R * W * d) + v * R * e_i } + e_i
            grad[0] = -j_11 + 1; grad[1] = -j_12; grad[2] = -j_13;
            grad[8] = -j_21; grad[9] = -j_22 + 1; grad[10] = -j_23;
            grad[16] = -j_31; grad[17] = -j_32; grad[18] = -j_33 + 1;

            // partial derivative ∂T/∂s = λ * R * d
            grad[3] = lam * rd_1;
            grad[11] = lam * rd_2;
            grad[19] = lam * rd_3;

            // partial derivative ∂T/∂ϑ = v * λ * ψ * R * ∂W/∂ϑ * d
            k = vp * lam; // v * ψ * λ
            grad[4] = k * rg_1;
            grad[12] = k * rg_2;
            grad[20] = k * rg_3;

            // partial derivative ∂T/∂φ = v * λ * ψ * R * ∂W/∂φ * d
            grad[5] = k * rh_1;
            grad[13] = k * rh_2;
            grad[21] = k * rh_3;

            // partial derivative ∂T/∂ψ = v * λ * R * W * d
            k = v * lam; // v * λ
            grad[6] = k * rwd_1;
            grad[14] = k * rwd_2;
            grad[22] = k * rwd_3;

            // partial derivative ∂T/∂a = λ' * r * ((s - 1) * R * d + v * ψ * R * W * d)
            k = lamDer * r; // λ' * r
            m = vp * k; // v * ψ * λ' * r
            k = k * sme; // λ' * r * (s - 1)
            grad[7] = k * rd_1 + m * rwd_1;
            grad[15] = k * rd_2 + m * rwd_2;
            grad[23] = k * rd_3 + m * rwd_3;
        }

        // --- Value ---
        Point3D value = new Point3D(v * rd_1 + c1, v * rd_2 + c2, v * rd_3 + c3);

        return new Transformation3D.Result(value, gradient, jacobi);

    }

    /**
     * Changes the parameters of this transformation.
     *
     * @param theta New parameters for this transformation. θ = [c1, c2, c3, s, ϑ, φ, ψ, a]
     */
    @Override
    public void changeParameters(double[] theta) {
        this.c1 = theta[0];
        this.c2 = theta[1];
        this.c3 = theta[2];
        this.sme = theta[3] - 1;
        this.a = theta[7];
        double cosTheta = Math.cos(theta[4]); // cos(ϑ)
        double sinTheta = Math.sqrt(1 - cosTheta * cosTheta); // sin(ϑ)
        this.w3 = Math.cos(theta[5]); // cos(φ)
        this.ww_33 = w3 * w3;
        this.sinPhi = Math.sqrt(1 - ww_33); // sin(φ)   NOTE: this must be a class field only if gradients will be calculated
        this.w1 = cosTheta * sinPhi;
        this.w2 = sinTheta * sinPhi;
        this.psi = theta[6];

        this.ww_11 = w1 * w1;
        this.ww_12 = w1 * w2;
        this.ww_13 = w1 * w3;
        this.ww_22 = w2 * w2;
        this.ww_23 = w2 * w3;

        this.sinThetaCosPhi = 0; // sin(ϑ) * cos(φ)   NOTE: needed only if gradients will be calculated
        this.cosThetaCosPhi = 0; // cos(ϑ) * cos(φ)   NOTE: needed only if gradients will be calculated
    }

    /**
     * Returns the number of parameters (8) by which this transformation is parametrised.
     *
     * @return Number of parameters (dimensionality of parameter space), which is constant 8 for this transformation.
     */
    @Override
    public int numberOfParameters() {
        return 8;
    }

}
