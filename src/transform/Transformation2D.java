package transform;

import gridfun.Array2D;
import util.Point2D;
import util.SimpsonAutomata;

import java.util.Arrays;

/**
 * Parametrised transformation from R² to R².
 */
public interface Transformation2D {

    /**
     * Returns the result of applying this transformation to a given point.
     * @param u A point, at which this transformation should be evaluated.
     * @return The result of applying this transformation to a given input point.
     */
    Point2D map(Point2D u);

    /**
     * Applies the transformation to a given input point and returns the resulting point together with
     * gradient ∂T(u;θ)/∂θ or Jacobi matrix ∂T(u;θ)/∂u, if specified.
     * @param u A point, at which the transformation and its derivatives should be evaluated.
     * @param calculateGradient Indicates whether the gradient ∂T(u;θ)/∂θ should be calculated.
     * @param calculateJacobi Indicates whether the Jacobi matrix ∂T(u;θ)/∂u should be calculated.
     * @return Result of this transformation at given input point, containing the value and derivatives.
     * If {@code calculateGradient} or {@code calculateJacobi} is false, then respective field in
     * {@code Transformation2D.Result} will be null.
     */
    Transformation2D.Result map(Point2D u, boolean calculateGradient, boolean calculateJacobi);

    /**
     * Changes the parameters of this transformation.
     * @param theta New parameters for this transformation.
     */
    void changeParameters(double[] theta);

    /**
     * Returns number of parameters by which this transformation is parametrised. Should always return the same
     * positive integer.
     * @return Number of parameters (dimensionality of parameter space).
     */
    int numberOfParameters();

    /**
     * Creates new transformation, which is the composition of this and of given transformation.
     * @param g Transformation to be composed by this transformation.
     * @return Composite transformation f ⚬ g, where f is this transformation.
     */
    default Transformation2D compose(Transformation2D g) {

        final Transformation2D f = this;

        return new Transformation2D() {
            @Override
            public Point2D map(Point2D u) {
                return f.map(g.map(u));
            }

            @Override
            public Transformation2D.Result map(Point2D u, boolean calculateGradient, boolean calculateJacobi) {
                Result gResult = g.map(u, calculateGradient, calculateJacobi);
                Result fResult = f.map(gResult.value, calculateGradient, true);
                double[] jf = fResult.jacobi.values;

                Array2D jacobi = null;
                if (calculateJacobi) {
                    double[] jg = fResult.jacobi.values;
                    double[] jacobiValues = new double[]{
                            jf[0] * jg[0] + jf[1] * jg[2], jf[0] * jg[1] + jf[1] * jg[3],
                            jf[2] * jg[0] + jf[3] * jg[2], jf[2] * jg[1] + jf[3] * jg[3]};
                    jacobi = new Array2D(2, 2, jacobiValues);
                }

                Array2D gradient = null;
                if (calculateGradient) {
                    int nf = f.numberOfParameters();
                    int ng = g.numberOfParameters();
                    int n = nf + ng;
                    double[] df = fResult.gradient.values;
                    double[] dg = gResult.gradient.values;
                    double[] gradientValues = new double[n + n];
                    System.arraycopy(df, 0, gradientValues, 0, nf);
                    System.arraycopy(df, nf, gradientValues, n, nf);
                    for (int i1 = nf, i2 = n + nf, k1 = 0, k2 = ng; i1 < n; i1++, i2++, k1++, k2++) {
                        gradientValues[i1] = jf[0] * dg[k1] + jf[1] * dg[k2];
                        gradientValues[i2] = jf[2] * dg[k1] + jf[3] * dg[k2];
                    }
                    gradient = new Array2D(2, n, gradientValues);
                }

                return new Result(fResult.value, gradient, jacobi);
            }

            @Override
            public void changeParameters(double[] theta) {
                int s = f.numberOfParameters();
                f.changeParameters(Arrays.copyOfRange(theta, 0, s));
                g.changeParameters(Arrays.copyOfRange(theta, s, theta.length));
            }

            @Override
            public int numberOfParameters() {
                return f.numberOfParameters() + g.numberOfParameters();
            }
        };
    }

    /**
     * Class to store the value T(u;θ) and derivatives ∂T(u;θ)/∂θ and ∂T(u;θ)/∂u of a parametrised
     * transformation T : ℝ^2 × ℝ^k -> ℝ^2.
     */
    class Result {

        /**
         * Value T(u;θ).
         */
        public final Point2D value;

        /**
         * Derivative of T with respect to parameter vector: ∂T(u;θ)/∂θ. If T = [T_x, T_y], then
         * ∂T(x,y;θ)/∂θ = [∂T_x/∂θ_1, ∂T_x/∂θ_2, ..., ∂T_x/∂θ_n; ∂T_y/∂θ_1, ∂T_y/∂θ_2, ... ∂T_y/∂θ_n]. Here n is
         * the dimensionality (length) of vector θ.
         */
        public final Array2D gradient;

        /**
         * Derivative of T with respect to a point: ∂T(u;θ)/∂u. Also known as Jacobi matrix.
         * If T = [T_x, T_y], then Jacobi matrix is J_T(x, y) = [∂T_x/∂x, ∂T_x/∂y; ∂T_y/∂x, ∂T_y/∂y].
         */
        public final Array2D jacobi; // derivative of T with respect to a point: ∂T(u;θ)/∂u


        public Result(double x, double y, Array2D gradient, Array2D jacobi) {
            this.value = new Point2D(x, y);
            this.gradient = gradient;
            this.jacobi = jacobi;
        }

        public Result(Point2D value, Array2D gradient, Array2D jacobi) {
            this.value = value;
            this.gradient = gradient;
            this.jacobi = jacobi;
        }

    }

    /**
     * Calculates the diagonal elements of (diagonal) precondition matrix of this transformation at current parameters.
     * This matrix is used in optimisation procedures, when searching for optimal transformation, to speed up
     * convergence.
     *
     * This method integrates numerically (using the Simpson quadrature formula) the square of norm of gradient
     * ||∂T(u;θ)/∂θ||² of this transformation at current parameter over a rectangular domain [0, l_x]×[0, l_y].
     * The diagonal elements p_i are then computed by formula
     * p_i =  (l_x * l_y) / ∫ ||∂T(u;θᵢ)/∂θᵢ||² dA(u).
     *
     * @param lx Upper bound of integration domain in x-direction.
     * @param ly Upper bound of integration domain in y-direction.
     * @param m Number of division points in x-direction.
     * @param n Number of division points in y-direction.
     * @return Array with diagonal elements of precondition matrix of this transformation at current parameters.
     */
    default double[] getPreconditionMatrix(double lx, double ly, int m, int n) {
        int mm = 2 * m;
        int nn = 2 * n;
        int p = numberOfParameters();
        double hx = lx / mm;
        double hy = ly / nn;

        double[] sum = new double[p];

        double x = 0;
        double y = 0;
        SimpsonAutomata simpsonAutomata = new SimpsonAutomata();
        for (int i = 0; i <= mm; i++) {
            boolean lastRow = (i == m);
            for (int j = 0; j <= nn; j++) {
                Transformation2D.Result r = map(new Point2D(x, y), true, false);
                int w = simpsonAutomata.progress((j == nn) || (lastRow && (j == 0)));
                for (int k = 0; k < p; k++) {
                    double gx = r.gradient.values[k];
                    double gy = r.gradient.values[k + p];
                    sum[k] += w * (gx * gx + gy * gy);
                }
                y += hy;
            }
            y = 0;
            x += hx;
        }

        double c = 36 * m * n; // = (lx * ly) / (hx * xy / 9), factored out, using hx = lx/(2*m) and hy = ly/(2*n)
        for (int k = 0; k < p; k++) { // here we calculate (lx * ly) / ((hx * xy / 9) * sum) = area / integral
            sum[k] = c / sum[k];
        }
        return sum;
    }

}
