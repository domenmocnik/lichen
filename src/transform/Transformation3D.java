package transform;

import gridfun.Array2D;
import util.Point3D;

import java.util.Arrays;

/**
 * Parametrised transformation from R³ to R³.
 */
public interface Transformation3D {

    /**
     * Returns the result of applying this transformation to a given point.
     * @param u A point, at which this transformation should be evaluated.
     * @return The result of applying this transformation to a given input point.
     */
    Point3D map(Point3D u);

    /**
     * Applies the transformation to a given input point and returns the resulting point together with
     * gradient ∂T(u;θ)/∂θ or Jacobi matrix ∂T(u;θ)/∂u, if specified.
     * @param u A point, at which the transformation and its derivatives should be evaluated.
     * @param calculateGradient Indicates whether the gradient ∂T(u;θ)/∂θ should be calculated.
     * @param calculateJacobi Indicates whether the Jacobi matrix ∂T(u;θ)/∂u should be calculated.
     * @return Result of this transformation at given input point, containing the value and derivatives.
     * If {@code calculateGradient} or {@code calculateJacobi} is false, then respective field in
     * {@code Transformation3D.Result} will be null.
     */
    Transformation3D.Result map(Point3D u, boolean calculateGradient, boolean calculateJacobi);

    /**
     * Changes the parameters of this transformation.
     * @param theta New parameters for this transformation.
     */
    void changeParameters(double[] theta);

    /**
     * Returns number of parameters by which this transformation is parametrised. Should always return the same
     * positive integer.
     * @return Number of parameters (dimensionality of parameter space).
     */
    int numberOfParameters();

    /**
     * Creates new transformation, which is the composition of this and of given transformation.
     * @param g Transformation to be composed by this transformation.
     * @return Composite transformation f ⚬ g, where f is this transformation.
     */
    default Transformation3D compose(Transformation3D g) {

        final Transformation3D f = this;

        return new Transformation3D() {

            @Override
            public Point3D map(Point3D u) {
                return f.map(g.map(u));
            }

            @Override
            public Transformation3D.Result map(Point3D u, boolean calculateGradient, boolean calculateJacobi) {
                Result gResult = g.map(u, calculateGradient, calculateJacobi);
                Result fResult = f.map(gResult.value, calculateGradient, true);
                double[] jf = fResult.jacobi.values;

                Array2D jacobi = null;
                if (calculateJacobi) {
                    double[] jg = fResult.jacobi.values;
                    double[] jacobiValues = new double[]{
                            jf[0] * jg[0] + jf[1] * jg[3] + jf[2] * jg[6],
                            jf[0] * jg[1] + jf[1] * jg[4] + jf[2] * jg[7],
                            jf[0] * jg[2] + jf[1] * jg[5] + jf[2] * jg[8],
                            jf[3] * jg[0] + jf[4] * jg[3] + jf[5] * jg[6],
                            jf[3] * jg[1] + jf[4] * jg[4] + jf[5] * jg[7],
                            jf[3] * jg[2] + jf[4] * jg[5] + jf[5] * jg[8],
                            jf[6] * jg[0] + jf[7] * jg[3] + jf[8] * jg[6],
                            jf[6] * jg[1] + jf[7] * jg[4] + jf[8] * jg[7],
                            jf[6] * jg[2] + jf[7] * jg[5] + jf[8] * jg[8]};
                    jacobi = new Array2D(3, 3, jacobiValues);
                }

                Array2D gradient = null;
                if (calculateGradient) {
                    int nf = f.numberOfParameters(); // TODO: mogoče se da izognit temu računanju vsakokrat?
                    int ng = g.numberOfParameters();
                    int n = nf + ng;
                    double[] df = fResult.gradient.values;
                    double[] dg = gResult.gradient.values;
                    double[] gradientValues = new double[3 * n];
                    System.arraycopy(df, 0, gradientValues, 0, nf);
                    System.arraycopy(df, nf, gradientValues, n, nf);
                    System.arraycopy(df, 2 * nf, gradientValues, 2 * n, nf);
                    for (int i1 = nf, i2 = n + nf, i3 = 2 * n + nf, k1 = 0, k2 = ng, k3 = 2 * ng;
                         i1 < n;
                         i1++, i2++, i3++, k1++, k2++, k3++) {
                        gradientValues[i1] = jf[0] * dg[k1] + jf[1] * dg[k2] + jf[2] * dg[k3];
                        gradientValues[i2] = jf[3] * dg[k1] + jf[4] * dg[k2] + jf[5] * dg[k3];
                        gradientValues[i3] = jf[6] * dg[k1] + jf[7] * dg[k2] + jf[8] * dg[k3];
                    }
                    gradient = new Array2D(3, n, gradientValues);
                }

                return new Result(fResult.value, gradient, jacobi);
            }

            @Override
            public void changeParameters(double[] theta) {
                int s = f.numberOfParameters();
                f.changeParameters(Arrays.copyOfRange(theta, 0, s));
                g.changeParameters(Arrays.copyOfRange(theta, s, theta.length));
            }

            @Override
            public int numberOfParameters() {
                return f.numberOfParameters() + g.numberOfParameters();
            }
        };
    }


    /**
     * Class to store the value T(u;θ) and derivatives ∂T(u;θ)/∂θ and ∂T(u;θ)/∂u of a parametrised
     * transformation T : ℝ^3 × ℝ^k -> ℝ^3.
     */
    class Result {

        /**
         * Value T(u;θ).
         */
        public final Point3D value;

        /**
         * Derivative of T with respect to parameter vector: ∂T(u;θ)/∂θ. If T = [T_x, T_y, T_z], then
         * ∂T(x,y;θ)/∂θ = [∂T_x/∂θ_1, ..., ∂T_x/∂θ_n; ∂T_y/∂θ_1, ..., ∂T_y/∂θ_n; ∂T_z/∂θ_1, ..., ∂T_z/∂θ_n]. Here n is
         * the dimensionality (length) of vector θ.
         */
        public final Array2D gradient;

        /**
         * Derivative of T with respect to a point: ∂T(u;θ)/∂u. Also known as Jacobi matrix.
         * If T = [T_x, T_y, T_z], then Jacobi matrix is
         * J_T(x, y, z) = [∂T_x/∂x, ∂T_x/∂y, ∂T_x/∂z; ∂T_y/∂x, ∂T_y/∂y, ∂T_y/∂z; ∂T_z/∂x, ∂T_z/∂y, ∂T_z/∂z].
         */
        public final Array2D jacobi; // derivative of T with respect to a point: ∂T(u;θ)/∂u


        public Result(double x, double y, double z, Array2D gradient, Array2D jacobi) {
            this.value = new Point3D(x, y, z);
            this.gradient = gradient;
            this.jacobi = jacobi;
        }

        public Result(Point3D value, Array2D gradient, Array2D jacobi) {
            this.value = value;
            this.gradient = gradient;
            this.jacobi = jacobi;
        }

    }

}
