package display;

import gridfun.GridFunction2D;
import javafx.event.EventHandler;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import java.nio.IntBuffer;

import com.sun.javafx.sg.prism.NGImageView;
import com.sun.javafx.sg.prism.NGNode;
import com.sun.prism.Graphics;
import com.sun.prism.Texture;
import com.sun.prism.impl.BaseResourceFactory;
import com.sun.prism.Image;

/**
 * Class for creating sample images, which will be displayed as "lichen on the bark".
 * EXAMPLE can be found here: http://docs.oracle.com/javafx/2/image_ops/jfxpub-image_ops.htm
 */
public class GridFunctionView extends ImageView {

    private final GridFunction2D gf;
    private final double min; // minimal value in gf
    private final double max; // maximal value in gf
    private final double intensityScale; // 256 / (max - min)
    private final PixelWriter pixelWriter;
    private double viewScale;

    private final double ED = 1.1;
    private final double DE = -10d / 11d;

    public GridFunctionView(GridFunction2D gf) {
        super();
        if (gf == null) {
            throw new NullPointerException("Grid function given in constructor of GridFunctionView should not be null");
        }
        this.gf = gf;
        // find min and max value in gf and calculate intensityScale
        double[] minmax = gf.minAndMaxValue();
        this.min = minmax[0];
        this.max = minmax[1];
        this.intensityScale = 256 / (max - min);
        // create pixel writer
        WritableImage writableImage = new WritableImage(gf.sizeY, gf.sizeX);
        this.pixelWriter = writableImage.getPixelWriter();
        WritablePixelFormat<IntBuffer> pixelFormat = PixelFormat.getIntArgbInstance();
        final int[] buffer = getBuffer(gf, min, max, intensityScale);
        pixelWriter.setPixels(0, 0, gf.sizeY, gf.sizeX, pixelFormat, buffer, 0, gf.sizeY);
        // the rest of work
        setImage(writableImage);
        setSmooth(false);
        this.viewScale = 3; // FIXME: set to 1
        super.setFitWidth(gf.sizeY * gf.spacingY * viewScale);
        super.setFitHeight(gf.sizeX * gf.spacingX * viewScale);
        this.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            double w = event.getX();
            double h = event.getY();
            int x = (int)(h / (gf.spacingX * viewScale));
            int y = (int)(w / (gf.spacingY * viewScale));
            double alpha = 0.5;
            int gray = (int)((gf.values[x * gf.sizeY + y] - min) * intensityScale * alpha);
            int imagePixel = 0xFF000000 | ((gray + 127) << 16) | (gray << 8) | gray;
            //pixelWriter.setArgb(y, x, imagePixel);
            pixelWriter.setArgb(y, x, 0xFFFF0000);
        });
        this.setOnScroll(event -> {
            //setTranslateX(getTranslateX() + event.getDeltaY());
            double amount = event.getDeltaY() / 40d;
            double scale = amount > 0 ? amount * ED : amount * DE;
            setScaleX(getScaleX() * scale);
            setScaleY(getScaleY() * scale);
        });
    }

    public GridFunctionView(String fileName) {
        this(io.Image2D.readLCH(fileName));
    }

    private WritableImage fromGridFunction(GridFunction2D gf) {
        if (gf == null) {
            throw new NullPointerException("Grid function given in constructor of GridFunctionView should not be null");
        }
        WritableImage writableImage = new WritableImage(gf.sizeY, gf.sizeX);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        WritablePixelFormat<IntBuffer> pixelFormat = PixelFormat.getIntArgbInstance();
        final int[] buffer = getBuffer(gf, min, max, intensityScale);
        pixelWriter.setPixels(0, 0, gf.sizeY, gf.sizeX, pixelFormat, buffer, 0, gf.sizeY);
        return writableImage;
    }

    private static int[] getBuffer(GridFunction2D gf, double min, double max, double scale) {
        int[] buffer = new int[gf.values.length];
        // calculate pixels from values
        for (int i = 0; i < gf.values.length; i++) {
            if (gf.values[i] >= max) {
                buffer[i] = 0xFFFFFFFF;
            } else if (gf.values[i] < min) {
                buffer[i] = 0xFF000000;
            } else {
                int s = (int) ((gf.values[i] - min) * scale); // intensity in [0, 255]
                buffer[i] = 0xFF000000 | (s << 16) | (s << 8) | s;
            }
        }
        return buffer;
    }




    /*
    * This part is needed to gain a nearest-neighbour interpolation ability in ImageView.
    * It is a code rewrite from Stackoverflow answer from user Martin Sojka, found here:
    * https://stackoverflow.com/questions/16089304/javafx-imageview-without-any-smoothing
    * */
    @SuppressWarnings("restriction")
    @Override protected NGNode impl_createPeer() {
        return new NGImageView() {
            private Image image;

            @Override public void setImage(Object img) {
                super.setImage(img);
                image = (Image) img;
            }

            @Override protected void renderContent(Graphics g) {
                BaseResourceFactory factory = (BaseResourceFactory) g.getResourceFactory();
                Texture tex = factory.getCachedTexture(image, Texture.WrapMode.CLAMP_TO_EDGE);
                tex.setLinearFiltering(false);
                tex.unlock();
                super.renderContent(g);
            }
        };
    }


}