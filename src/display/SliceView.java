package display;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.image.*;

import java.nio.IntBuffer;

class SliceView extends ImageView {

    private final int[] buffer;
    private final PixelWriter pixelWriter;
    private final WritablePixelFormat<IntBuffer> pixelFormat = PixelFormat.getIntArgbInstance();

    private final int dim1; // first dimension of an image (in pixels)
    private final int dim2; // second dimension of an image (in pixels)
    private final int bufferStride; // buffer stride, it is equal to dim1 * dim2
    private final int lastSlice; // number of the last slice, which is (number of all slices - 1)
    private final SimpleIntegerProperty slice; // current slice number
    private int offset; // offset into buffer

    private double dragOriginX = 0; // used to record the coordinates of position where mouse drag started
    private double dragOriginY = 0;

    /**
     * Construct a new slice view from given data.
     *
     * This slice view can be used to view a 3D image slice by slice. It displays a k-th slice of dimensions dim1 x dim2
     * from pixel colors given in a buffer from position k*dim1*dim2 to position (k+1)*dim1*dim2.
     *
     * @param buffer A buffer of pixel colors.
     * @param dim1 Number of pixels in vertical direction.
     * @param dim2 Number of pixels in horizontal direction.
     * @param scale1 Vertical pixel size.
     * @param scale2 Horizontal pixel size.
     */
    SliceView(int[] buffer, int dim1, int dim2, double scale1, double scale2) {
        super();
        this.buffer = buffer;
        this.dim1 = dim1;
        this.dim2 = dim2;
        this.bufferStride = dim1 * dim2;
        this.lastSlice = buffer.length / bufferStride - 1;
        this.slice = new SimpleIntegerProperty(lastSlice / 2);
        this.offset = slice.get() * bufferStride;

        WritableImage writableImage = new WritableImage(dim2, dim1);
        this.pixelWriter = writableImage.getPixelWriter();
        pixelWriter.setPixels(0, 0, dim2, dim1, pixelFormat, buffer, offset, dim2);
        setImage(writableImage);
        setSmooth(false);
        super.setFitHeight(dim1 * scale1);
        super.setFitWidth(dim2 * scale2);

        this.setOnScroll(event -> {
            //setTranslateX(getTranslateX() + event.getDeltaY());
            int amount = (int) event.getDeltaY() / 40;
            int newSlice = slice.get() + amount;
            slice.set(newSlice);
        });

        slice.addListener((o, oldSlice, newSlice) -> {
            if (newSlice.intValue() < 0) {
                slice.set(0);
            } else if (newSlice.intValue() > lastSlice) {
                slice.set(lastSlice);
            } else {
                slice.set(newSlice.intValue());
            }
            offset = slice.get() * bufferStride;
            pixelWriter.setPixels(0, 0, dim2, dim1, pixelFormat, buffer, offset, dim2);
        });

        this.setOnMousePressed(event -> {
            dragOriginX = event.getSceneX();
            dragOriginY = event.getSceneY();
        });

        this.setOnMouseDragged(event -> {
            double x = event.getSceneX();
            double y = event.getSceneY();
            double draggedDistanceX = x - dragOriginX;
            double draggedDistanceY = y - dragOriginY;
            if (event.isPrimaryButtonDown()) {
                this.setTranslateX(this.getTranslateX() + draggedDistanceX);
                this.setTranslateY(this.getTranslateY() + draggedDistanceY);
            } else if (event.isSecondaryButtonDown()) {
                double beta = this.getScaleX() + draggedDistanceX / 80;
                if (beta < 0.1) {
                    beta = 0.1;
                }
                this.setScaleY(beta);
                this.setScaleX(beta);
            }
            dragOriginX = x;
            dragOriginY = y;
        });
    }

    /**
     * Gets the current slice number. The slice numbering starts at 0 and ends at (number of all slices - 1).
     * @return Current slice number.
     */
    int getSliceNumber() {
        return this.slice.get();
    }

    /**
     * Sets the current slice and displays it.
     *
     * @param newSlice The slice to be displayed.
     */
    void setSliceNumber(int newSlice) {
        slice.set(newSlice);
    }

    /**
     * Gets the slice property. The slice numbering starts at 0 and ends at (number of all slices - 1).
     *
     * This property can be used to change the slice to be displayed, or to get the current slice number, or to
     * add a ChangeListener to it.
     *
     * @return The slice property of this slice view.
     */
    SimpleIntegerProperty getSliceProperty() {
        return this.slice;
    }

    /**
     * Gets the buffer of this slice view. The buffer can not be reassigned, but it's values may be changed.
     *
     * @return The buffer of this slice view.
     */
    int[] getBuffer() {
        return this.buffer;
    }

    /**
     * Refreshes the display if the values in the buffer have been changed.
     */
    void refresh() {
        pixelWriter.setPixels(0, 0, dim2, dim1, pixelFormat, buffer, offset, dim2);
    }
}
