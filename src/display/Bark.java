package display;

import gridfun.GridFunction2D;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


/**
 * Graphical user interface for displaying images (and more in the future ...).
 */
public class Bark extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Here follows your code that generates GridFunction2D that you want to view:
        // GridFunction2D gf;

        // Could be anything as simple as just reading image from file, like this:
        // gf = io.Image2D.readLCH("/home/domen/IdeaProjects/lichen/res/mrslice.lch");

        // ... or call a method from your examples folder, like so:
        // gf = examples.ExampleClass.methodThatReturnsGridFunction();

        // ... or so (if you want to show multiple GridFunctions):
        GridFunction2D[] gfs = examples.LocallyAffineRegistration.start3();

        // Include views in a GridPane (with 3 rows):
        GridPane pane = new GridPane();
        for (int i = 0; i < gfs.length; i++) {
            GridFunctionView gfView = new GridFunctionView(gfs[i]);
            pane.add(gfView, i < 3 ? 0 : 1, i % 3);
        }

        /*// Create GridFunctionViews for each GridFunction2D:
        GridFunctionView gfView0 = new GridFunctionView(gfs[0]);
        GridFunctionView gfView1 = new GridFunctionView(gfs[1]);
        GridFunctionView gfView2 = new GridFunctionView(gfs[2]);

        // Include views in a FlowPane:
        Pane pane = new FlowPane(gfView0, gfView1, gfView2);*/

        // Here follows the rest of the code, which does not need to be changed.
        pane.setPrefSize(200, 200);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.getIcons().add(new Image("file:/home/domen/IdeaProjects/lichen/res/lichen.png"));
        primaryStage.setTitle("Bark");
        primaryStage.show();
    }

}
