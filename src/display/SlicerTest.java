package display;

import gridfun.GridFunction3D;
import io.Image2D;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import transform.LocallySimilarityTransform3D;
import transform.Rigid3D;
import transform.Transformation3D;
import util.Point3D;

public class SlicerTest extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        //GridFunction3D gfun = Image2D.read3DImageFromLCH("C:\\Users\\Domen\\IdeaProjects\\lichen\\res\\mr_image.lch");
        GridFunction3D gfun = Image2D.read3DImageFromLCH("/home/domen/IdeaProjects/lichen/res/Medical_images/img5mr1.lch");
        Slicer slicer = new Slicer(gfun);
        Pane pane = new FlowPane(
                new SlicePane(slicer.getSagittalSliceView()),
                new SlicePane(slicer.getCoronalSliceView()),
                new SlicePane(slicer.getAxialSliceView()));
        //pane.setPrefSize(200, 200);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        //primaryStage.getIcons().add(new Image("file:C:\\Users\\Domen\\IdeaProjects\\lichen\\res\\lichen.png"));
        primaryStage.getIcons().add(new Image("file:/home/domen/IdeaProjects/lichen/res/lichen.png"));
        primaryStage.setTitle("Bark");
        primaryStage.show();
    }

    private static class SlicePane extends Pane {

        private final SliceView sliceView;

        private SlicePane(SliceView sliceView) {
            super(sliceView);
            this.sliceView = sliceView;
            //setPrefSize(500,300);
            setStyle("-fx-background-color: grey;");
        }
    }

    /*
    @Override
    public void start(Stage primaryStage) {
        //GridFunction3D gfun = Image2D.read3DImageFromLCH("C:\\Users\\Domen\\IdeaProjects\\lichen\\res\\mr_image.lch");
        GridFunction3D gfun = Image2D.read3DImageFromLCH("/home/domen/IdeaProjects/lichen/res/mr_image.lch");
        Slicer slicer = new Slicer(gfun);
        Point3D center = new Point3D(gfun.lengthX / 2, gfun.lengthY / 2, gfun.lengthZ / 2);
        //Transformation3D transform = new Rigid3D(center, false);
        //double[] theta0 = new double[] {-Math.PI / 2, Math.PI / 2, Math.PI / 6, 12, -5, 17};
        Transformation3D transform = new LocallySimilarityTransform3D(x -> Math.atan(x) / x, null);
        double[] theta0 = new double[] {center.x, center.y, center.z, 1.3, -Math.PI / 2, Math.PI / 2, Math.PI / 6, 0.2}; //θ = [c1, c2, c3, s, ϑ, φ, ψ, a]^T
        transform.changeParameters(theta0);
        GridFunction3D moved = gfun.transform(transform);
        Slicer slicer2 = new Slicer(moved);
        //Pane pane = new FlowPane(slicer.getSagittalSliceView(), slicer.getCoronalSliceView(), slicer.getAxialSliceView());
        GridPane pane = new GridPane();
        pane.add(slicer.getSagittalSliceView(), 0, 0);
        pane.add(slicer.getCoronalSliceView(), 0, 1);
        pane.add(slicer.getAxialSliceView(), 0, 2);
        pane.add(slicer2.getSagittalSliceView(), 1, 0);
        pane.add(slicer2.getCoronalSliceView(), 1, 1);
        pane.add(slicer2.getAxialSliceView(), 1, 2);
        //pane.setPrefSize(200, 200);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        //primaryStage.getIcons().add(new Image("file:C:\\Users\\Domen\\IdeaProjects\\lichen\\res\\lichen.png"));
        primaryStage.getIcons().add(new Image("file:/home/domen/IdeaProjects/lichen/res/lichen.png"));
        primaryStage.setTitle("Bark");
        primaryStage.show();
    }
    */
}
