package display;

import gridfun.GridFunction3D;
// import org.controlsfx.control.RangeSlider;

/**
 * A class to display 3D images slice-wise.
 */
public class Slicer {

    private final GridFunction3D gfun;
    private double minValue; // lower treshold value for values in grid function
    private double maxValue; // upper treshold value for values in grid function
    private double intensityScale; // 256 / (maxValue - minValue)

    private final SliceView sagittalSliceView;
    private final SliceView coronalSliceView;
    private final SliceView axialSliceView;

    //private final RangeSlider rangeSlider; TODO: put rangeSlider up to work


    public Slicer(GridFunction3D gfun) {
        this.gfun = gfun;
        double[] minmax = gfun.minAndMaxValue();
        this.minValue = minmax[0];
        this.maxValue = minmax[1]; // set to 2000 for CT image case 5
        // TODO: instead of min-max windowing, do a windowing that starts at 5th and ends at 95th percentil
        this.intensityScale = 256 / (maxValue - minValue);
        int[][] buffers = getBuffers(gfun, minValue, maxValue, intensityScale);
        double scale = 2;
        double scaleX = gfun.spacingX * scale;
        double scaleY = gfun.spacingY * scale;
        double scaleZ = gfun.spacingZ * scale;
        sagittalSliceView = new SliceView(buffers[0], gfun.sizeZ, gfun.sizeY, scaleZ, scaleY);
        coronalSliceView = new SliceView(buffers[1], gfun.sizeZ, gfun.sizeX, scaleZ, scaleX);
        axialSliceView = new SliceView(buffers[2], gfun.sizeY, gfun.sizeX, scaleY, scaleX);

        /*this.rangeSlider = new RangeSlider(minValue, maxValue, minValue, maxValue);
        rangeSlider.setShowTickMarks(true);
        rangeSlider.setShowTickLabels(true);
        rangeSlider.setBlockIncrement(10);*/
    }

    public SliceView getSagittalSliceView() {
        return sagittalSliceView;
    }

    public SliceView getCoronalSliceView() {
        return coronalSliceView;
    }

    public SliceView getAxialSliceView() {
        return axialSliceView;
    }

    private static int[][] getBuffers(GridFunction3D gf, double min, double max, double scale) {
        final int n = gf.values.length;
        int[][] buffers = new int[3][n];

        int[] values = new int[n];
        for (int i = 0; i < n; i++) {
            if (gf.values[i] >= max) {
                values[i] = 0xFFFFFFFF;
            } else if (gf.values[i] < min) {
                values[i] = 0xFF000000;
            } else {
                int s = (int) ((gf.values[i] - min) * scale); // intensity in [0, 255]
                values[i] = 0xFF000000 | (s << 16) | (s << 8) | s;
            }
        }

        int[] sagittalBuffer = buffers[0];
        int lic = 0;
        for (int i = 0; i < gf.sizeX; i++) {
            for (int k = 0; k < gf.sizeZ; k++) {
                for (int j = 0; j < gf.sizeY; j++) {
                    sagittalBuffer[lic] = values[(i * gf.sizeY + j) * gf.sizeZ + k];
                    lic++;
                }
            }
        }

        int[] coronalBuffer = buffers[1];
        lic = 0;
        for (int j = gf.sizeY - 1; j >= 0; j--) {
            for (int k = 0; k < gf.sizeZ; k++) {
                for (int i = 0; i < gf.sizeX; i++) {
                    coronalBuffer[lic] = values[(i * gf.sizeY + j) * gf.sizeZ + k];
                    lic++;
                }
            }
        }

        int[] axialBuffer = buffers[2];
        lic = 0;
        for (int k = 0; k < gf.sizeZ; k++) {
            for (int j = 0; j < gf.sizeY; j++) {
                for (int i = 0; i < gf.sizeX; i++) {
                    axialBuffer[lic] = values[(i * gf.sizeY + j) * gf.sizeZ + k];
                    lic++;
                }
            }
        }

        return buffers;
    }
}
