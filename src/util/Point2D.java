package util;

/**
 * A point in 2-dimensional Euclidean space.
 */
public class Point2D {

    public final double x;
    public final double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
