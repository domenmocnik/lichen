package util;

/**
 * Exception used to indicate that provided point does not lie in considered domain.
 */
public class OutOfGridException extends Exception {

    public OutOfGridException() {}

    public OutOfGridException(String message) {
        super(message);
    }

}
