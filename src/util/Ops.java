package util;

/**
 * Operators on arrays.
 */
public class Ops {

    public static double[] neg(double[] a) {
        double[] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = -a[i];
        }
        return result;
    }

    public static double[] mul(double s, double[] a) {
        double[] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = s * a[i];
        }
        return result;
    }

    public static double[] sum(double[] a, double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Array lengths do not match.");
        }
        double[] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[i] + b[i];
        }
        return result;
    }

    public static double[] mul(double[] a, double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Array lengths do not match.");
        }
        double[] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[i] * b[i];
        }
        return result;
    }

    public static double[] inv(double[] a) {
        double[] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = 1 / a[i];
        }
        return result;
    }

    public static double[] sub(double[] a, double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Array lengths do not match.");
        }
        double[] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[i] - b[i];
        }
        return result;
    }

    public static double dot(double[] a, double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Array lengths do not match.");
        }
        double result = 0;
        for (int i = 0; i < a.length; i++) {
            result += a[i] * b[i];
        }
        return result;
    }

    public static double dist2(double[] a, double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Array lengths do not match.");
        }
        double result = 0;
        for (int i = 0; i < a.length; i++) {
            double d = a[i] - b[i];
            result += d * d;
        }
        return Math.sqrt(result);
    }

    public static double[] line(double[] a, double t, double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Array lengths do not match.");
        }
        double[] result = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[i] + t * b[i];
        }
        return result;
    }

}
