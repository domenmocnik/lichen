package util;

/**
 * A pair of value and gradient of a scalar function.
 */
public class ValueGradientPair {

    public final double value;
    public final double[] gradient;

    public ValueGradientPair(double value, double[] gradient) {
        this.value = value;
        this.gradient = gradient;
    }

}
