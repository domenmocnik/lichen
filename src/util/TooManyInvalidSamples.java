package util;

/**
 * Exception thrown when too many sample points from reference image domain map outside of moving image domain.
 */
public class TooManyInvalidSamples extends RuntimeException {

    public TooManyInvalidSamples() {
        super();
    }

    public TooManyInvalidSamples(int samples, int invalid, double requiredRatio) {
        super("Too many sample points from reference image domain map outside of moving image domain.\n" +
                "Number of sample points: " + samples + "\n" +
                "Number of invalid sample points: " + invalid + "\n" +
                "Ratio of valid sample points: " + ((double)(samples - invalid) / (double)samples) +
                " (required: " + requiredRatio + ")");
    }
}
