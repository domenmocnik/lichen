package util;

/**
 * Simpson automaton generates weight coefficients for integration with Simpson quadrature formulae in
 * 2D rectangular domain.
 */
public class SimpsonAutomata {

    // States of the automata:
    // last byte (last two hex-digits) represents the value of the current state (the weight we need for integration)
    // second last byte represents the index of the next state, if flag is false
    // first byte represents the index of the next state, if flag is true
    // second byte represents the value of the current state, if flag is true
    private final static int[] states = new int[]{
            0x01_01_01_01, // the START state
            0x02_04_02_04,
            0x03_01_01_02,
            0x04_04_04_04,
            0x05_10_05_10,
            0x06_04_04_08,
            0x01_01_07_02,
            0x08_08_08_08,
            0x03_02_07_04
    };

    private int currentState;

    /**
     * Creates new Simpson automata.
     */
    public SimpsonAutomata() {
        this.currentState = 0;
    }

    /**
     * Returns the value in the current state and progresses to the next state
     * @param flag Indicates into which state to progress and which value to return from current state.
     * @return The value produced by the current state.
     */
    public int progress(boolean flag) {
        int s = states[currentState];
        if (flag) {
            currentState = (s & 0xFF000000) >>> 24;
            return (s & 0x00FF0000) >> 16;
        } else {
            currentState = (s & 0x0000FF00) >> 8;
            return s & 0x000000FF;
        }
    }


    public static void main(String[] args) {
        SimpsonAutomata sa = new SimpsonAutomata();
        for (int i = 0; i <= 10; i++) {
            boolean lastRow = i == 10;
            for (int j = 0; j <= 14; j++) {
                System.out.print(sa.progress((j == 14) || (lastRow && (j == 0))) + " ");
            }
            System.out.println();
        }
    }
}
