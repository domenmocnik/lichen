package util;

import gridfun.Array2D;

import java.util.Arrays;

/**
 * Static functions for various interpolation tasks.
 */
public class Interpolation {


    /**
     * Calculates coefficients of cubic B-spline, which interpolates values on 1D grid.
     *
     * @param values grid values to be interpolated with cubic B-spline
     * @return coefficients of cubic B-spline, which interpolates given values
     */
    public static double[] cubicBSplineTransform(double[] values) {
        final int n = values.length;
        double[] a = new double[n];
        double[] b = Arrays.stream(values).map(x -> 6 * x).toArray();
        double[] c = new double[n + 2];
        // Forward filtering
        a[0] = 2;
        b[0] = b[0] / 2;
        for (int i = 1; i < n - 1; i++) {
            double factor = -1 / a[i - 1];
            a[i] = 4 + factor;
            b[i] += factor * b[i - 1];
        }
        double factor = -2 / a[n - 2];
        a[n - 1] = 4 + factor;
        b[n - 1] += factor * b[n - 2];
        // Backwards filtering
        c[n] = b[n - 1] / a[n - 1];
        for (int i = n - 2; i > 0; i--) {
            c[i + 1] = (b[i] - c[i + 2]) / a[i];
        }
        c[0] = c[2];
        c[n + 1] = c[n - 1];

        return c;
    }


    /**
     * Calculates coefficients of cubic B-spline, which interpolates values on 2D grid.
     *
     * @param a grid values to be interpolated with cubic B-spline
     * @return coefficients of cubic B-spline, which interpolates given values
     */
    public static Array2D cubicBSplineTransform2D(Array2D a) {
        // Apply 1D transform row-wise
        Array2D intermediate = new Array2D(a.sizeX, a.sizeY + 2);
        for (int i = 0; i < a.sizeX; i++) {
            intermediate.setRow(i, cubicBSplineTransform(a.getRow(i)));
        }
        // Apply 1D transform column-wise
        Array2D result = new Array2D(a.sizeX + 2, a.sizeY + 2);
        for (int j = 0; j < result.sizeY; j++) {
            result.setColumn(j, cubicBSplineTransform(intermediate.getColumn(j)));
        }
        return result;
    }


    /**
     * DeBoor's algorithm for calculating the value of cubic B-spline at some point x from [0, 1].
     *
     * @param x Number from interval [0, 1], where value of cubic B-spline should be calculated.
     * @param c Coefficients of B-spline. The length of this array must be exactly 4.
     * @return Value of cubic B-spline at x.
     */
    public static double deBoor3(final double x, final double[] c) {
        // coefficients c^[1](x)
        c[0] = (2 * c[1] + c[0] + x * (c[1] - c[0])) / 3;
        c[1] = (c[2] + 2 * c[1] + x * (c[2] - c[1])) / 3;
        c[2] = c[2] + x * (c[3] - c[2]) / 3;
        // coefficients c^[2](x)
        c[0] = (c[1] + c[0] + x * (c[1] - c[0])) / 2;
        c[1] = c[1] + x * (c[2] - c[1]) / 2;
        // coefficient c^[3](x) = s(x)
        return c[0] + x * (c[1] - c[0]);
    }


    /**
     * DeBoor's algorithm for calculating the value and derivative of cubic B-spline at some point x from [0, 1].
     *
     * @param x Number from interval [0, 1], where value and derivative of cubic B-spline should be calculated.
     * @param c Coefficients of B-spline. The length of this array must be exactly 4.
     * @return Value and derivative of cubic B-spline at x. (Array of length 2)
     * NOTE: If your grid spacing is different than 1, then you should divide derivative with grid spacing to get
     * proper derivative.
     */
    public static double[] deBoor3withDerivative(final double x, final double[] c) {
        double[] d = {c[1] - c[0], c[2] - c[1], c[3] - c[2]};
        // coefficients c^[1](x)
        c[0] = (2 * c[1] + c[0] + x * d[0]) / 3;
        c[1] = (c[2] + 2 * c[1] + x * d[1]) / 3;
        c[2] = c[2] + x * d[2] / 3;
        // coefficients c^[2](x)
        c[0] = (c[1] + c[0] + x * (c[1] - c[0])) / 2;
        c[1] = c[1] + x * (c[2] - c[1]) / 2;
        // coefficient c^[3](x) = s(x)
        c[0] = c[0] + x * (c[1] - c[0]);

        // Calculating derivative s'(x):
        // coefficients d^[1](x)
        d[0] = (d[1] + d[0] + x * (d[1] - d[0])) / 2;
        d[1] = d[1] + x * (d[2] - d[1]) / 2;
        // coefficient d^[2](x) = s'(x)
        d[0] = d[0] + x * (d[1] - d[0]);

        return new double[]{c[0], d[0]};
    }

    /**
     * DeBoor's algorithm for calculating the value of quadric B-spline at some point x from [0, 1].
     *
     * @param x Number from interval [0, 1], where value of quadratic B-spline should be calculated.
     * @param c Coefficients of B-spline. The length of this array must be exactly 3.
     * @return Value of quadratic B-spline at x.
     */
    public static double deBoor2(final double x, final double[] c) {
        // coefficients c^[1](x)
        c[0] = (c[1] + c[0] + x * (c[1] - c[0])) / 2;
        c[1] = c[1] + x * (c[2] - c[1]) / 2;
        // coefficient c^[2](x) = s(x)
        return c[0] + x * (c[1] - c[0]);
    }


    /**
     * Calculates values of basis cubic B-splines at x from interval [0, 1].
     * Method returns array of length 4 containing values of basis splines.
     *
     * @param x Value from interval [0, 1], where values of basis splines need to be calculated.
     * @return Array of length 4 containing values of basis splines.
     */
    public static double[] coxDeBoor3(final double x) {
        double emx = 1 - x;
        double[] b = new double[4];
        // first column
        b[2] = emx;
        b[3] = x;
        // second column
        b[1] = emx * b[2] / 2;
        b[2] = b[3] + (b[2] + x * (b[2] - b[3])) / 2;
        b[3] = x * b[3] / 2;
        // third column
        b[0] = emx * b[1] / 3;
        b[1] = (2 * (b[1] + b[2]) + x * (b[1] - b[2])) / 3;
        b[2] = b[3] + (b[2] + x * (b[2] - b[3])) / 3;
        b[3] = x * b[3] / 3;
        return b;
    }


    /**
     * Calculates values and derivatives of basis cubic B-splines at x from interval [0, 1].
     * Method returns array of length 8. First 4 elements are values and last 4 elements are derivatives
     * of basis splines.
     *
     * @param x Value from interval [0, 1], where values and derivatives of basis splines need to be calculated.
     * @return Array of length 8. First 4 elements are values and last 4 elements are derivatives of basis splines.
     */
    public static double[] coxDeBoor3WithDerivatives(final double x) {
        double emx = 1 - x;
        double[] b = new double[8]; // first 4 elements are values, last 4 elements are derivatives
        // first column
        b[2] = emx;
        b[3] = x;
        // second column
        b[1] = emx * b[2] / 2;
        b[2] = b[3] + (b[2] + x * (b[2] - b[3])) / 2;
        b[3] = x * b[3] / 2;
        // derivatives (need to be scaled after this function ends, if interval width is not 1)
        b[4] = b[0] - b[1];
        b[5] = b[1] - b[2];
        b[6] = b[2] - b[3];
        b[7] = b[3];
        // third column
        b[0] = emx * b[1] / 3;
        b[1] = (2 * (b[1] + b[2]) + x * (b[1] - b[2])) / 3;
        b[2] = b[3] + (b[2] + x * (b[2] - b[3])) / 3;
        b[3] = x * b[3] / 3;
        return b;
    }
}
