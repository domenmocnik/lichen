package costfun;

import util.Interpolation;
import util.ValueGradientPair;
import java.util.Arrays;

/**
 * Mutual information calculator. Uses cubic Parzen-window for moving image values and
 * linear Parzen-window for reference image values.
 */
class MutualInformationWorkerCuLi {

    // number of bins for image histograms
    private final int binsR;
    private final int binsM;
    // width of histogram interval
    private final double epsR;
    private final double epsM;

    // minimum and maximum image intensity values
    private final double minR;
    private final double maxR;
    private final double minM;
    private final double maxM;

    private final int numpar; // dimensionality of deformation model (number of parameters)

    private final double[] hJ; // values of reference and moving image joint histogram; 2D array of size (binsR x binsM)
    private final double[] hD; // values of gradient joint histogram; 3D array of size (binsR x binsM x numpar)

    private int cardinality; // number of samples included in histograms


    /**
     * Constructor for MutualInformationWorkerCuLi.
     *
     * NOTE: The caller of this constructor should take care of validity of input arguments. These checks are not
     * performed inside of this constructor, because it is optimised for performance. Particular checks to be made are:
     * bins >= 2, minR <= maxR, minM <= maxM and numpar >= 1.
     *
     * @param bins Number of bins per histogram dimension.
     * @param minR Minimal intensity value in reference image.
     * @param maxR Maximal intensity value in reference image.
     * @param minM Minimal intensity value in moving image.
     * @param maxM Maximal intensity value in moving image.
     * @param numpar Number of parameters in transformation (dimensionality of transformation model).
     */
    MutualInformationWorkerCuLi(int bins, double minR, double maxR, double minM, double maxM, int numpar) {
        // All checks for input arguments must be done before calling this constructor!
        this.binsR = bins + 1;
        this.binsM = bins + 3;
        this.epsR = (maxR - minR) / bins;
        this.epsM = (maxM - minM) / bins;
        this.minR = minR;
        this.maxR = maxR;
        this.minM = minM;
        this.maxM = maxM;
        this.numpar = numpar;
        this.hJ = new double[binsR * binsM];
        this.hD = new double[binsR * binsM * numpar];
        this.cardinality = 0;
    }

    /**
     * Includes reference and moving image values of some pixel/voxel in histogram of this MI.
     * Gradient is not included; it is assumed that gradient is 0. (Use method includeAcorn() to include gradient too).
     * This methods assumes that input intensity values are in range of provided min and max values.
     *
     * @param valR Reference image value to be included.
     * @param valM Moving image value to be included.
     */
    void includeValues(double valR, double valM) {

        double r = (valR - minR) / epsR;
        int fromR = (int) r;
        if (valR == maxR) { fromR--; }
        double x = r - fromR;
        double[] refCont = {1 - x, x};

        r = (valM - minM) / epsM; // FIXME: Bug! a.valM can be <minM or >maxM when using cubic interpolation.
        int fromM = (int) r;
        if (valM == maxM) { fromM--; }
        x = r - fromM;
        double[] movCont = Interpolation.coxDeBoor3(x);

        int lib0 = fromR * binsM + fromM; // linear index of [fromR, fromM + j], j will be loop counter
        int lib1 = lib0 + binsM; // linear index of [fromR + 1, fromM + j]
        for (int j = 0; j < 4; j++) {
            hJ[lib0] += refCont[0] * movCont[j];
            hJ[lib1] += refCont[1] * movCont[j];
            lib0++;
            lib1++;
        }

        cardinality++;
    }

    /**
     * Includes values and gradient of some pixel/voxel in histograms of this MI.
     * This methods assumes that input intensity values are in range of provided min and max values.
     *
     * @param a Acorn to be included. Acorn is a synonym for triple (reference value, moving value, moving gradient).
     */
    void includeAcorn(Acorn a) {

        double r = (a.valR - minR) / epsR;
        int fromR = (int) r;
        if (a.valR == maxR) { fromR--; }
        double x = r - fromR;
        double[] refCont = {1 - x, x};

        r = (a.valM - minM) / epsM; // FIXME: Bug! a.valM can be <minM or >maxM when using cubic interpolation.
        int fromM = (int) r;
        if (a.valM == maxM) { fromM--; }
        x = r - fromM;
        double[] movCont = Interpolation.coxDeBoor3WithDerivatives(x); // derivatives are not yet divided with epsM here
        // they are included in hD raw, but are divided with epsM together later at the end of method "evaluateWithGradient"

        int lib0 = fromR * binsM + fromM; // linear index of [fromR, fromM + j], j will be loop counter
        int lib1 = lib0 + binsM; // linear index of [fromR + 1, fromM + j]
        int lic0 = lib0 * numpar; // linear index of [fromR, fromM + j, k], j and k will be loop counters
        int lic1 = lib1 * numpar; // linear index of [fromR + 1, fromM + j, k]
        for (int j = 0; j < 4; j++) {
            hJ[lib0] += refCont[0] * movCont[j];
            hJ[lib1] += refCont[1] * movCont[j];
            for (int k = 0; k < numpar; k++) {
                double s = movCont[4 + j] * a.grad[k];
                hD[lic0] += refCont[0] * s;
                hD[lic1] += refCont[1] * s;
                lic0++;
                lic1++;
            }
            lib0++;
            lib1++;
        }

        cardinality++;
    }

    /**
     * Evaluates the value of the mutual information from accumulated image value pairs.
     *
     * @return Value of the mutual information, calculated from accumulated image value pairs.
     */
    double evaluate() {
        if (cardinality == 0) { throw new IllegalStateException("No pixels/voxels were included yet."); }
        // normalization of joint histogram & computation of marginal probabilities
        final double[] pR = new double[binsR]; // marginal probabilities of reference image
        final double[] pM = new double[binsM]; // marginal probabilities of moving image
        int lib = 0; // linear index of [i, j]
        for (int i = 0; i < binsR; i++) {
            for (int j = 0; j < binsM; j++) {
                hJ[lib] /= cardinality; // normalization, to get proper probabilities
                pR[i] += hJ[lib];
                pM[j] += hJ[lib];
                lib++;
            }
        }
        // calculation of value
        double value = 0;
        lib = 0;
        for (int i = 0; i < binsR; i++) {
            if (pR[i] == 0.0) { // here hR[i][j] == 0 for all j and there is nothing to do
                lib += binsM;
                continue;
            }
            for (int j = 0; j < binsM; j++) {
                if (hJ[lib] == 0.0) { // only positive values contribute to the sum
                    lib++;
                    continue;
                }
                value += hJ[lib] * Math.log(hJ[lib] / (pR[i] * pM[j]));
                lib++;
            }
        }
        return -value; // negative of mutual information is actually what we need
    }

    /**
     * Evaluates the value and the gradient of the mutual information from accumulated acorns.
     *
     * @return Value and gradient of the mutual information, calculated from accumulated acorns.
     */
    ValueGradientPair evaluateWithGradient() {
        if (cardinality == 0) { throw new IllegalStateException("No pixels/voxels were included yet."); }
        // normalization of joint histogram & computation of marginal probabilities
        final double[] pR = new double[binsR]; // marginal probabilities of reference image
        final double[] pM = new double[binsM]; // marginal probabilities of moving image
        int lib = 0; // linear index of [i, j]
        for (int i = 0; i < binsR; i++) {
            for (int j = 0; j < binsM; j++) {
                hJ[lib] /= cardinality; // normalization, to get proper probabilities
                pR[i] += hJ[lib];
                pM[j] += hJ[lib];
                lib++;
            }
        }
        // calculation of value and gradient
        double value = 0;
        double[] gradient = new double[numpar];
        lib = 0;
        int lic = 0; // linear index of [i, j, k]
        for (int i = 0; i < binsR; i++) {
            if (pR[i] == 0.0) { // here hR[i][j] == 0 for all j and there is nothing to do
                lib += binsM;
                lic = lib * numpar;
                continue;
            }
            final double logpr = Math.log(pR[i]);
            for (int j = 0; j < binsM; j++) {
                if (hJ[lib] == 0.0) { // only positive values contribute to the sum
                    lib++;
                    lic += numpar;
                    continue;
                }
                final double factor = Math.log(hJ[lib] / pM[j]);
                value += hJ[lib] * (factor - logpr);
                for (int k = 0; k < numpar; k++) {
                    gradient[k] += hD[lic] * factor;
                    lic++;
                }
                lib++;
            }
        }
        value = -value; // negative of mutual information is actually what we need
        // normalization of gradient
        final double c = -1 / (cardinality * epsM);
        for (int k = 0; k < numpar; k++) {
            gradient[k] *= c;
        }
        return new ValueGradientPair(value, gradient);
    }

    void clear() {
        Arrays.fill(hJ, 0);
        Arrays.fill(hD, 0);
        cardinality = 0;
    }

    void join(MutualInformationWorkerCuLi other) {
        Arrays.setAll(this.hJ, i -> this.hJ[i] + other.hJ[i]);
        Arrays.setAll(this.hD, i -> this.hD[i] + other.hD[i]);
        this.cardinality += other.cardinality;
        // other.clear();
    }


}
