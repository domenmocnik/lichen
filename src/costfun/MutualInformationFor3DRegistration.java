package costfun;

import gridfun.GridFunction3D;
import gridfun.Interpolator3D;
import gridfun.TrilinearInterpolator;
import transform.Transformation3D;
import util.OutOfGridException;
import util.Point3D;
import util.ValueGradientPair;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Class to represent a mutual information between the reference and moving 3D-images.
 * Uses linear Parzen window for smoothing both the moving and reference image values.
 *
 * This class is capable to calculate the value of mutual information only, so if you need to calculate a gradient
 * too, use a different class, preferably one that uses higher-order Parzen-window for moving image values, to get
 * continuous derivatives.
 */
public class MutualInformationFor3DRegistration implements CostFunction {

    private final GridFunction3D referenceIm; // reference image
    private final Interpolator3D movingIm; // moving image
    private final Transformation3D transformation; // transformation from reference image domain to moving image domain

    private final Point3D[] domainPoints; // array of points from domain of reference grid-function

    private final double defaultMovingValue; // default intensity value for out-of-grid points
    // minimum and maximum image intensity values:
    private final double minR;
    private final double maxR;
    private final double minM;
    private final double maxM;

    // number of bins for image histograms:
    private final int binsR; // number of bins in histogram of reference image
    private final int binsM; // number of bins in histogram of moving image
    // width of histogram interval:
    private final double epsR;
    private final double epsM;
    private final int histogramSize; // length of an array that stores values of joint histogram

    private final Worker headWorker; /* parent worker that distributes the entire work of evaluation to the child
                                         workers and merges their partial work together */
    private final ExecutorService threadPool; // a pool of threads that will execute tasks of workers


    public MutualInformationFor3DRegistration(GridFunction3D referenceImage, GridFunction3D movingImage,
                             Transformation3D transformation, int bins) {
        if (referenceImage == null || movingImage == null || transformation == null) {
            throw new IllegalArgumentException("Input arguments for construction " +
                    "of the MutualInformation should not be null.");
        }
        if (bins < 2) {
            throw new IllegalArgumentException("Number of bins must be at least 2.");
        }
        this.referenceIm = referenceImage;
        this.movingIm = new TrilinearInterpolator(movingImage);
        this.transformation = transformation;

        this.domainPoints = referenceIm.getDomainPoints();

        double[] minMaxR = referenceImage.minAndMaxValue();
        double[] minMaxM = movingImage.minAndMaxValue();
        this.defaultMovingValue = minMaxM[0];
        this.minR = minMaxR[0];
        this.maxR = minMaxR[1];
        this.minM = minMaxM[0];
        this.maxM = minMaxM[1];

        this.binsR = bins + 1;
        this.binsM = bins + 1;
        this.epsR = (maxR - minR) / bins;
        this.epsM = (maxM - minM) / bins;
        this.histogramSize = binsR * binsM;

        int numberOfWorkers = Runtime.getRuntime().availableProcessors();
        this.threadPool = Executors.newFixedThreadPool(numberOfWorkers);
        int pointsPerWorker = referenceImage.values.length / numberOfWorkers + 1;
        this.headWorker = this.new Worker(0, referenceImage.values.length, pointsPerWorker);
    }


    /**
     * A worker that does a portion of evaluation of mutual information. These workers are used to split
     * the evaluation on smaller parts, that can be run in parallel.
     */
    private class Worker implements Runnable {

        private final double[] hJ; // storage for joint histogram; considered as 2D array of size (binsR x binsM)
        private int cardinality; // number of samples included in histograms
        private final int start; // (inclusive) index where this worker starts
        private final int end; // (exclusive) index where this worker stops
        private final ListIterator<Worker> childIterator; // iterator over workers that have been spawned
                                                          // by this worker
        private final Deque<Future<?>> futures; // stack of futures which monitor the execution of worker's tasks


        /**
         * Constructs a worker which is responsible for processing domain points, that lie in array between
         * respective start and end index. If the amount of points is too big for a single worker, then this worker
         * will split the work between itself and other workers.
         *
         * @param start The index of the beginning of domain-points-array segment that should be processed.
         * @param end The end index of domain-points-array segment that should be processed.
         * @param pointsPerWorker The upper limit for the amount of points that should be processed by a single worker.
         */
        private Worker(int start, int end, int pointsPerWorker) {
            this.hJ = new double[histogramSize];
            this.cardinality = 0;

            LinkedList<Worker> childWorkers = new LinkedList<>();
            int amountOfPoints = end - start;
            while (amountOfPoints > pointsPerWorker) { // if the amount of work for this worker is too big, split it
                int middle = start + (amountOfPoints / 2);
                // creates and adds new worker
                Worker child = MutualInformationFor3DRegistration.this.new Worker(middle, end, pointsPerWorker);
                childWorkers.add(child);
                end = middle;
                amountOfPoints = end - start;
            }
            this.start = start;
            this.end = end;

            this.childIterator = childWorkers.listIterator(0);
            this.futures = new LinkedList<>();
        }


        /**
         * When a worker's thread is started (by invoking a method start()), this method is invoked, which first starts
         * child workers, then does its own bulk of work by processing its own segment and finally merges the results
         * of child workers.
         */
        @Override
        public void run() {
            clear();
            // submit child workers to threads:
            while (childIterator.hasNext()) {
                Worker w = childIterator.next();
                Future<?> m = threadPool.submit(w);
                futures.addFirst(m);
            }

            // doing own part of the job:
            for (int i = start; i < end; i++) {
                double valR = referenceIm.values[i];
                double valM;
                try {
                    valM = movingIm.valueAt(transformation.map(domainPoints[i]));
                } catch (OutOfGridException e) {
                    valM = defaultMovingValue;
                }
                includeValues(valR, valM);
            }

            // merging:
            while (childIterator.hasPrevious()) {
                Worker w = childIterator.previous();
                Future<?> m = futures.removeFirst();
                try {
                    m.get(); // here this thread waits for worker w to finish its job
                } catch (InterruptedException e) {
                    System.out.println("The computation was cancelled unexpectedly.");
                    e.printStackTrace();
                    System.exit(9090);
                } catch (ExecutionException e) {
                    System.out.println("The computation threw an exception.");
                    e.printStackTrace();
                    System.exit(9090);
                }
                merge(w);
            }
        }

        /**
         * Includes a pair of reference and moving image values of some pixel/voxel in histogram of this MI.
         * This methods assumes that input intensity values are in range of provided min and max values.
         *
         * @param valR Reference image value to be included.
         * @param valM Moving image value to be included.
         */
        private void includeValues(double valR, double valM) {

            double r = (valR - minR) / epsR;
            int fromR = (int) r;
            if (valR == maxR) { fromR--; }
            double x = r - fromR;
            double refCont0 = 1 - x;
            double refCont1 = x;

            r = (valM - minM) / epsM; // FIXME: Bug! a.valM can be <minM or >maxM when using cubic interpolation.
            int fromM = (int) r;
            if (valM == maxM) { fromM--; }
            x = r - fromM;
            double movCont0 = 1 - x;
            double movCont1 = x;

            int lib0 = fromR * binsM + fromM; // linear index of [fromR, fromM + j], j is either 0 or 1
            int lib1 = lib0 + binsM; // linear index of [fromR + 1, fromM + j]
            hJ[lib0] += refCont0 * movCont0;
            hJ[lib1] += refCont1 * movCont0;
            lib0++;
            lib1++;
            hJ[lib0] += refCont0 * movCont1;
            hJ[lib1] += refCont1 * movCont1;

            cardinality++;
        }

        /**
         * Evaluates the value of the mutual information from accumulated image value pairs.
         *
         * @return Value of the mutual information, calculated from accumulated image value pairs.
         */
        private double evaluate() {
            if (cardinality == 0) { throw new IllegalStateException("No pixels/voxels were included yet."); }
            // normalization of joint histogram & computation of marginal probabilities
            final double[] pR = new double[binsR]; // marginal probabilities of reference image
            final double[] pM = new double[binsM]; // marginal probabilities of moving image
            int lib = 0; // linear index of [i, j]
            for (int i = 0; i < binsR; i++) {
                for (int j = 0; j < binsM; j++) {
                    hJ[lib] /= cardinality; // normalization, to get proper probabilities
                    pR[i] += hJ[lib];
                    pM[j] += hJ[lib];
                    lib++;
                }
            }
            // calculation of value
            double value = 0;
            lib = 0;
            for (int i = 0; i < binsR; i++) {
                if (pR[i] == 0.0) { // here hR[i][j] == 0 for all j and there is nothing to do
                    lib += binsM;
                    continue;
                }
                for (int j = 0; j < binsM; j++) {
                    if (hJ[lib] == 0.0) { // only positive values contribute to the sum
                        lib++;
                        continue;
                    }
                    value += hJ[lib] * Math.log(hJ[lib] / (pR[i] * pM[j]));
                    lib++;
                }
            }
            return -value; // negative of mutual information is actually what we need
        }

        /**
         * Clears histogram an sets the cardinality to 0.
         */
        private void clear() {
            Arrays.fill(hJ, 0);
            cardinality = 0;
        }

        /**
         * Sums the joint histograms of both workers (element-wise) and sums their cardinalities.
         *
         * @param other A worker to be joined with this worker.
         */
        private synchronized void merge(Worker other) {
            Arrays.setAll(this.hJ, i -> this.hJ[i] + other.hJ[i]);
            this.cardinality += other.cardinality;
        }

    }


    /**
     * Returns the value of this cost function at the given input parameter.
     *
     * @param theta Input parameter.
     * @return Value of this cost function at the given input parameter.
     */
    @Override
    public double getValueAt(double[] theta) {
        transformation.changeParameters(theta);
        Future<?> m = threadPool.submit(headWorker);
        try {
            m.get();
        } catch (InterruptedException e) {
            System.out.println("The computation was cancelled unexpectedly.");
            e.printStackTrace();
            System.exit(9090);
        } catch (ExecutionException e) {
            System.out.println("The computation threw an exception.");
            e.printStackTrace();
            System.exit(9090);
        }
        return headWorker.evaluate();
    }

    /**
     * This function is not implemented for this implementation of mutual information, because it is not differentiable
     * on the whole domain. Do not use it. Throws UnsupportedOperationException.
     *
     * @param theta Input parameter.
     * @return Value and gradient of this cost function at the given input parameter.
     */
    @Override
    public ValueGradientPair getValueAndGradientAt(double[] theta) {
        throw new UnsupportedOperationException("This cost function does not support calculation of gradient.");
    }

    /**
     * Returns the dimensionality of the domain of this function.
     *
     * @return The dimensionality of the domain of this function.
     */
    @Override
    public int dimensionality() {
        return transformation.numberOfParameters();
    }

    /**
     * Shuts down the thread pool which is used for parallel evaluation of mutual information. This method should
     * be called always when an instance of this class is not needed anymore, otherwise the program will not terminate.
     */
    public void shutdown() {
        threadPool.shutdown();
    }

}
