package costfun;

import util.ValueGradientPair;

/**
 * An interface to represent differentiable cost function f: ℝ^n -> ℝ.
 */
public interface CostFunction {

    /**
     * Returns value of this cost function at the given input parameter.
     *
     * @param theta Input parameter.
     * @return Value of this cost function at the given input parameter.
     */
    double getValueAt(double[] theta);

    /**
     * Returns value and gradient of this cost function at the given parameter.
     *
     * @param theta Input parameter.
     * @return Value and gradient of this cost function at the given input parameter.
     */
    ValueGradientPair getValueAndGradientAt(double[] theta);

    /**
     * Returns the dimensionality of the domain of this function.
     *
     * @return The dimensionality of the domain of this function.
     */
    int dimensionality();

}
