package costfun;

import gridfun.BilinearInterpolator;
import gridfun.GridFunction2D;
import gridfun.Interpolator2D;
import transform.Transformation2D;
import util.OutOfGridException;
import util.Point2D;
import util.ValueGradientPair;

/**
 * Class to represent a mutual information between the reference and the moving 2D-images.
 * Uses cubic Parzen window for smoothing the histogram of moving image values and linear Parzen window for
 * smoothing the histogram of reference image values.
 */
public class MutualInformation implements CostFunction {

    private final GridFunction2D referenceIm;
    private final Interpolator2D movingIm;
    private final Transformation2D transformation;
    private final int numPar;
    private final Acorn[] acorns;
    private final MutualInformationWorkerCuLi worker;
    private final double defaultMovingValue; // default values for acorns of out-of-grid points

    public MutualInformation(GridFunction2D referenceImage, GridFunction2D movingImage,
                             Transformation2D transformation, int bins) {
        if (referenceImage == null || movingImage == null || transformation == null) {
            throw new IllegalArgumentException("Input arguments for construction " +
                    "of the MutualInformation should not be null.");
        }
        if (bins < 2) {
            throw new IllegalArgumentException("Number of bins must be at least 2.");
        }
        this.referenceIm = referenceImage;
        this.movingIm = new BilinearInterpolator(movingImage);
        this.transformation = transformation;
        this.numPar = transformation.numberOfParameters();
        this.acorns = new Acorn[referenceImage.values.length];
        double[] minMaxR = referenceImage.minAndMaxValue();
        double[] minMaxM = movingImage.minAndMaxValue();
        this.worker = new MutualInformationWorkerCuLi(bins, minMaxR[0], minMaxR[1], minMaxM[0], minMaxM[1], numPar);
        this.defaultMovingValue = minMaxM[0];
    }

    @Override
    public double getValueAt(double[] theta) {
        transformation.changeParameters(theta);
        worker.clear();
        // fetch image value pairs and include them in worker
        double xR = 0;
        double yR = 0;
        int j = 1;
        for (int lib = 0; lib < referenceIm.values.length; lib++) {
            Point2D transformedPoint = transformation.map(new Point2D(xR, yR));
            try {
                double valM = movingIm.valueAt(transformedPoint);
                worker.includeValues(referenceIm.values[lib], valM);
            } catch (OutOfGridException e) {
                worker.includeValues(referenceIm.values[lib], defaultMovingValue);
            }
            if (j < referenceIm.sizeY) {
                yR += referenceIm.spacingY;
                j++;
            } else {
                yR = 0;
                j = 1;
                xR += referenceIm.spacingX;
            }
        }
        return worker.evaluate();
    }

    @Override
    public ValueGradientPair getValueAndGradientAt(double[] theta) {
        transformation.changeParameters(theta);
        fetchAcorns();
        worker.clear();
        for (Acorn a : acorns) {
            worker.includeAcorn(a);
        }
        return worker.evaluateWithGradient();
    }

    @Override
    public int dimensionality() {
        return transformation.numberOfParameters();
    }

    private void fetchAcorns() {
        double xR = 0;
        double yR = 0;
        int j = 0;
        final int limit = referenceIm.sizeY - 1;
        for (int lib = 0; lib < referenceIm.values.length; lib++) {
            Transformation2D.Result transformResult = transformation.map(new Point2D(xR, yR), true, false);
            try {
                double[] imageResult = movingIm.valueAndGradientAt(transformResult.value);
                // multiply gradient of moving image and gradient of transformation
                double[] movGrad = new double[numPar];
                double[] g = transformResult.gradient.values;
                for (int k = 0, k2 = numPar; k < numPar; k++, k2++) {
                    movGrad[k] = imageResult[1] * g[k] + imageResult[2] * g[k2];
                }
                // insert new acorn
                acorns[lib] = new Acorn(referenceIm.values[lib], imageResult[0], movGrad);
            } catch (OutOfGridException e) {
                acorns[lib] = new Acorn(referenceIm.values[lib], defaultMovingValue, new double[numPar]);
            } finally {
                if (j < limit) {
                    yR += referenceIm.spacingY;
                    j++;
                } else {
                    yR = 0;
                    j = 0;
                    xR += referenceIm.spacingX;
                }
            }
        }
    }

}

/*
* ------ NOTES: ------
* Here are some methods, that you can use in the future when you decide to
*
* (1) parallelise evaluation of MI:
*
* private final Supplier<MutualInformationWorkerCuLi> supplier = () -> new MutualInformationWorkerCuLi(bins, minR, maxR, minM, maxM, numpar);
* public ValueGradientPair evaluate(double[] theta) {
*     transformation.changeParameters(theta);
*     outOfGridSamples = 0;
*     fetchAcornsRandomly();
*     return acorns.parallelStream().collect(
*             supplier,
*             MutualInformationWorkerCuLi::includeAcorn,
*             MutualInformationWorkerCuLi::join
*     ).evaluateWithGradient();
* }
*
* (2) randomize the selection of acorns:
*
* private void fetchAcornsRandomly() {
*     acorns.clear();
*     for (int i = 0; i < samplingSize; i++) {
*         int lib = (int) (Math.random() * referenceIm.values.length);
*         double xR = (lib / referenceIm.sizeY) * referenceIm.spacingX;
*         double yR = (lib % referenceIm.sizeY) * referenceIm.spacingY;
*         Transformation2D.Result transformResult = transformation.mapWithGradient(new Point2D(xR, yR));
*         try {
*             double[] imageResult = movingIm.getValueAndGradientAt(transformResult.value);
*             // multiply gradient of moving image and gradient of transformation
*             double[] movGrad = new double[numpar];
*             for (int j = 0; j < numpar; j++) {
*                 movGrad[j] = imageResult[1] * transformResult.dx[j] + imageResult[2] * transformResult.dy[j];
*             }
*             // insert new acorn
*             acorns.add(new Acorn(referenceIm.values[lib], imageResult[0], movGrad));
*         } catch (OutOfGridException e) {
*             outOfGridSamples++;
*         }
*     }
*     if (outOfGridSamples > threshold) {
*         throw new TooManyInvalidSamples(samplingSize, outOfGridSamples, requiredRatio);
*     }
* }
*/