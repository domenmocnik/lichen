package costfun;

/**
 * Structure for holding both values of reference and moving image at same voxel/pixel, as well as
 * gradient of moving image with respect to transformation parameters.
 */
class Acorn {

    final double valR; // intensity value of reference image in this pixel/voxel
    final double valM; // intensity value of moving image in this pixel/voxel
    final double[] grad; // gradient of moving image in this pixel/voxel with respect to transformation parameters

    Acorn(double valR, double valM, double[] grad) {
        this.valR = valR;
        this.valM = valM;
        this.grad = grad;
    }

}
