package examples;

import costfun.MutualInformation;
import gridfun.GridFunction2D;
import io.Image2D;
import optim.AcmConjugateGradientMethod;
import optim.ConjugateGradientMethod;
import org.apache.commons.math3.optim.*;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.gradient.Preconditioner;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;
import transform.LocallyAffine2D;
import transform.Transformation2D;
import util.Ops;

import java.util.Arrays;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Example for multiresolution locally affine registration.
 */
public class LocallyAffineRegistration {

    private static final double G = Math.PI / 180; // one angle degree in radians

    private static final DoubleUnaryOperator lambdaFun = x -> Math.atan(x) / x;
    private static final DoubleUnaryOperator lambdaDerivativeFun = x -> {
        double xx = x * x;
        return (x / (xx + 1) - Math.atan(x)) / xx;
    };
    private static final GridFunction2D originalFun = Image2D.readLCH(
            "/home/domen/IdeaProjects/lichen/res/mrslice.lch");
    private static final double px = originalFun.lengthX / 100.; // one percent of image height
    private static final double py = originalFun.lengthY / 100.; // one percent of image width

    private static String stringify(double[] array) {
        return Arrays.stream(array).mapToObj(x -> String.format("%.4f", x)).collect(Collectors.joining(", "));
    }

    public static GridFunction2D[] start() {

        LocallyAffine2D transform = new LocallyAffine2D(lambdaFun, lambdaDerivativeFun);

        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] thetaOriginal = new double[]{ originalFun.lengthX / 3., originalFun.lengthY / 4.,
                Math.PI / 4, 1.3, 0.9, Math.PI / 32, 0.5 };
        System.out.println("Original theta:");
        System.out.println(Arrays.toString(thetaOriginal));
        transform.changeParameters(thetaOriginal);
        GridFunction2D fMov = originalFun.transform(transform, false);
        GridFunction2D gMov = fMov.downsample();
        GridFunction2D gFix = originalFun.downsample();
        GridFunction2D hMov = gMov.downsample();
        GridFunction2D hFix = gFix.downsample();
        Supplier<double[]> sup = () -> transform.getPreconditionMatrix(originalFun.lengthX, originalFun.lengthY,
                200, 200);

        // level 0:
        MutualInformation mi = new MutualInformation(hMov, hFix, transform, 32);
        ConjugateGradientMethod optimiser = new ConjugateGradientMethod(mi);
        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] theta0 = new double[]{ fMov.lengthX / 3.08, fMov.lengthY / 4.21,
                Math.PI / 4.8, 1.5, 0.7, Math.PI / 16, 1 };
        transform.changeParameters(theta0);
        double[] thetaOpt = optimiser.optimise(theta0, sup, 8);
        System.out.println(Arrays.toString(thetaOpt));

        // level 1:
        mi = new MutualInformation(gMov, gFix, transform, 32);
        optimiser = new ConjugateGradientMethod(mi);
        thetaOpt = optimiser.optimise(thetaOpt, sup, 8);
        System.out.println(Arrays.toString(thetaOpt));

        // level 2:
        mi = new MutualInformation(fMov, originalFun, transform, 32);
        optimiser = new ConjugateGradientMethod(mi);
        thetaOpt = optimiser.optimise(thetaOpt, sup, 8);
        System.out.println(Arrays.toString(thetaOpt));

        transform.changeParameters(thetaOpt);
        return new GridFunction2D[]{originalFun, fMov, originalFun.transform(transform, false)};

    }

    /**
     * This method does the same as the first one, but uses publicly implemented optimisation algorithm from
     * the Apache Commons Math 3.6.1 package.
     * @return Several grid functions for display as a result.
     */
    public static GridFunction2D[] start2() {

        LocallyAffine2D transform = new LocallyAffine2D(lambdaFun, lambdaDerivativeFun);

        System.out.println("Original image resolution: " + originalFun.spacingX + " x " + originalFun.spacingY + " mm².");
        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] thetaOriginal = new double[]{ originalFun.lengthX / 2., originalFun.lengthY / 3.,
                22.5 * G, 1.3, 0.9, 45 * G, 0.3 };
        System.out.println("Original theta:   " + stringify(thetaOriginal));
        transform.changeParameters(thetaOriginal);
        GridFunction2D fMov = originalFun.transform(transform, false);
        GridFunction2D gMov = fMov.downsample();
        GridFunction2D gFix = originalFun.downsample();
        GridFunction2D hMov = gMov.downsample();
        GridFunction2D hFix = gFix.downsample();

        Preconditioner preconditioner = (point, direction) -> {
            transform.changeParameters(point);
            double[] diagonals = transform.getPreconditionMatrix(originalFun.lengthX, originalFun.lengthY,
                    200, 200);
            return Ops.mul(diagonals, direction);
        };

        // level 0:
        MutualInformation mi = new MutualInformation(hMov, hFix, transform, 32);
        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] theta0 = new double[]{ fMov.lengthX / 2. - 4., fMov.lengthY / 3. + 6.,
                25 * G, 1.5, 0.7, 40 * G, 0.5 };
        System.out.println("Initial theta:    " + stringify(theta0));
        // create optimiser
        AcmConjugateGradientMethod optimiser = new AcmConjugateGradientMethod(mi, theta0)
                .maxIter(8000)
                .relativeTolerance(1e-12)
                .absoluteTolerance(1e-12)
                .lineSearchParameters(1e-10, 1e-10, 10.0)
                .preconditioner(preconditioner);
        double[] thetaOpt = optimiser.optimise();
        System.out.println("Theta at level 0: " + Arrays.toString(thetaOpt));

        // level 1:
        mi = new MutualInformation(gMov, gFix, transform, 32);
        optimiser.costFunction(mi).initialGuess(thetaOpt);
        thetaOpt = optimiser.optimise();
        System.out.println("Theta at level 1: " + stringify(thetaOpt));

        // level 2:
        mi = new MutualInformation(fMov, originalFun, transform, 32);
        optimiser.costFunction(mi).initialGuess(thetaOpt);
        thetaOpt = optimiser.optimise();
        System.out.println("Theta at level 2: " + stringify(thetaOpt));

        // Create images for visualisation:
        GridFunction2D grid = originalFun.createDomainGrid(5);
        transform.changeParameters(thetaOpt);
        GridFunction2D imgTopt = originalFun.transform(transform, false);
        GridFunction2D gridTopt = grid.transform(transform, false);
        transform.changeParameters(thetaOriginal);
        // fMov is the same as: GridFunction2D imgTorig = originalFun.transform(transform, false);
        GridFunction2D gridTorig = grid.transform(transform, false);
        return new GridFunction2D[]{originalFun, fMov, imgTopt, grid, gridTorig, gridTopt};
    }

    /**
     * This example demonstrates usage of transformation, that is a composition of two locally affine transformations.
     *
     * @return Several grid functions for display as a result.
     */
    public static GridFunction2D[] start3() {

        LocallyAffine2D locAff1 = new LocallyAffine2D(lambdaFun, lambdaDerivativeFun);
        LocallyAffine2D locAff2 = new LocallyAffine2D(lambdaFun, lambdaDerivativeFun);
        Transformation2D transform = locAff2.compose(locAff1);

        System.out.println("Original image resolution: " + originalFun.spacingX +
                " x " + originalFun.spacingY + " mm².");
        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] thetaOriginal = new double[]{
                50 * px, 33 * py, 22.5 * G, 1.3, 0.9, 45 * G, 0.3,
                25 * px, 67 * py, 30 * G, 0.6, 1.1, -30 * G, 0.6};
        System.out.println("Original theta:   " + stringify(thetaOriginal));
        transform.changeParameters(thetaOriginal);
        GridFunction2D fMov = originalFun.transform(transform, false);
        GridFunction2D gMov = fMov.downsample();
        GridFunction2D gFix = originalFun.downsample();
        GridFunction2D hMov = gMov.downsample();
        GridFunction2D hFix = gFix.downsample();

        Preconditioner preconditioner = (point, direction) -> {
            transform.changeParameters(point);
            double[] diagonals = transform.getPreconditionMatrix(originalFun.lengthX, originalFun.lengthY,
                    200, 200);
            return Ops.mul(diagonals, direction);
        };

        // level 0:
        MutualInformation mi = new MutualInformation(hMov, hFix, transform, 32);
        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] theta0 = new double[]{
                50 * px - 4., 33 * py + 2., 25 * G, 1.5, 0.7, 40 * G, 0.5,
                25 * px + 5., 67 * py - 2, 25 * G, 0.8, 1, -26 * G, 0.5};
        System.out.println("Initial theta:    " + stringify(theta0));
        // create optimiser
        AcmConjugateGradientMethod optimiser = new AcmConjugateGradientMethod(mi, theta0)
                .maxIter(8000)
                .relativeTolerance(1e-12)
                .absoluteTolerance(1e-12)
                .lineSearchParameters(1e-10, 1e-10, 10.0)
                .preconditioner(preconditioner);
        double[] thetaOpt = optimiser.optimise();
        System.out.println("Theta at level 0: " + stringify(thetaOpt));

        // level 1:
        mi = new MutualInformation(gMov, gFix, transform, 48);
        optimiser.costFunction(mi).initialGuess(thetaOpt);
        thetaOpt = optimiser.optimise();
        System.out.println("Theta at level 1: " + stringify(thetaOpt));

        // level 2:
        mi = new MutualInformation(fMov, originalFun, transform, 64);
        optimiser.costFunction(mi).initialGuess(thetaOpt);
        thetaOpt = optimiser.optimise();
        System.out.println("Theta at level 2: " + stringify(thetaOpt));

        // Create images for visualisation:
        GridFunction2D grid = originalFun.createDomainGrid(5);
        transform.changeParameters(thetaOpt);
        GridFunction2D imgTopt = originalFun.transform(transform, false);
        GridFunction2D gridTopt = grid.transform(transform, false);
        transform.changeParameters(thetaOriginal);
        // fMov is the same as: GridFunction2D imgTorig = originalFun.transform(transform, false);
        GridFunction2D gridTorig = grid.transform(transform, false);
        return new GridFunction2D[]{originalFun, fMov, imgTopt, grid, gridTorig, gridTopt};
    }

    /**
     * In this example CMA-ES optimisation strategy is used.
     *
     * @return Several grid functions for display as a result.
     */
    public static GridFunction2D[] start4() {

        LocallyAffine2D locAff1 = new LocallyAffine2D(lambdaFun, lambdaDerivativeFun);
        LocallyAffine2D locAff2 = new LocallyAffine2D(lambdaFun, lambdaDerivativeFun);
        Transformation2D transform = locAff2.compose(locAff1);

        System.out.println("Original image resolution: " + originalFun.spacingX +
                " x " + originalFun.spacingY + " mm².");
        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] thetaOriginal = new double[]{
                50 * px, 33 * py, 22.5 * G, 1.3, 0.9, 45 * G, 0.3,
                25 * px, 67 * py, 30 * G, 0.6, 1.1, -30 * G, 0.6};
        System.out.println("Original theta: " + stringify(thetaOriginal));
        transform.changeParameters(thetaOriginal);
        GridFunction2D fMov = originalFun.transform(transform, false);

        // θ = [x₀, y₀, ϑ, s₁, s₂, φ, a]
        double[] theta0 = new double[]{
                50 * px - 4., 33 * py + 2., 25 * G, 1.5, 0.7, 40 * G, 0.5,
                25 * px + 5., 67 * py - 2, 25 * G, 0.8, 1, -26 * G, 0.5};
        MutualInformation mi = new MutualInformation(fMov, originalFun, transform, 48);
        System.out.println("Initial theta:  " + stringify(theta0) + "; value: " + mi.getValueAt(theta0));

        double stopFitness = Double.NEGATIVE_INFINITY;
        boolean isActiveCMA = true;
        int diagonalOnly = 0;
        int checkFeasibleCount = 1;
        RandomGenerator random = new Well44497b(); //Well19937c();
        boolean generateStatistics = false;
        SimplePointChecker<PointValuePair> convergenceChecker =
                new SimplePointChecker<>(1e-12, 1e-12);
        double[] lowerBounds = {theta0[0] - 10, theta0[1] - 10, theta0[2] - Math.PI / 4, theta0[3] / 3, theta0[4] / 3,
                theta0[5] - Math.PI / 4, theta0[6] / 3,
                theta0[7] - 10, theta0[8] - 10, theta0[9] - Math.PI / 4, theta0[10] / 3, theta0[11] / 3,
                theta0[12] - Math.PI / 4, theta0[13] / 3};
        double[] upperBounds = {theta0[0] + 10, theta0[1] + 10, theta0[2] + Math.PI / 4, theta0[3] * 3, theta0[4] * 3,
                theta0[5] + Math.PI / 4, theta0[6] * 3,
                theta0[7] + 10, theta0[8] + 10, theta0[9] + Math.PI / 4, theta0[10] * 3, theta0[11] * 3,
                theta0[12] + Math.PI / 4, theta0[13] * 3};
        SimpleBounds bounds = new SimpleBounds(lowerBounds, upperBounds);
        transform.changeParameters(theta0);
        double[] precond = transform.getPreconditionMatrix(originalFun.lengthX, originalFun.lengthY, 200, 400);
        double[] zigma = Ops.mul(1 / precond[0], precond);
        /*double[] zigma = {6, 6, Math.PI / 8, 0.3, 0.3, Math.PI / 8, 0.3,
                6, 6, Math.PI / 8, 0.3, 0.3, Math.PI / 8, 0.2,};*/
        /*ConvergenceChecker<PointValuePair> convergenceChecker = new ConvergenceChecker<PointValuePair>() {
            @Override
            public boolean converged(int i, PointValuePair pair1, PointValuePair pair2) {
                double[] r = Ops.mul(Ops.inv(precond), Ops.sub(pair1.getPoint(), pair2.getPoint()));
                double d = Arrays.stream(Ops.mul(r, r)).map(Math::sqrt).sum();
                double fd = Math.abs(pair1.getValue() - pair2.getValue());
                if (d < 1e-12 && fd < 1e-12) {
                    return true;
                } else {
                    return false;
                }
            }
        };*/
        OptimizationData sigma = new CMAESOptimizer.Sigma(Ops.mul(10, zigma));
        //System.out.println("sigma: " + stringify(Ops.mul(5, zigma)));
        OptimizationData popSize = new CMAESOptimizer.PopulationSize(150);
                //(int) (4 + Math.floor(3 * Math.log(thetaOriginal.length))));
        ObjectiveFunction objFun = new ObjectiveFunction(mi::getValueAt);

        CMAESOptimizer optimiser = new CMAESOptimizer(8000, stopFitness, isActiveCMA, diagonalOnly,
                checkFeasibleCount, random, generateStatistics, convergenceChecker);
        PointValuePair solution = optimiser.optimize(new InitialGuess(theta0), objFun,
                GoalType.MINIMIZE, bounds, sigma, popSize, new MaxEval(30000));

        System.out.println("Optimal theta:  " + stringify(solution.getPoint()) + "; value: " + solution.getValue());
        transform.changeParameters(solution.getPoint());
        GridFunction2D imgTopt = originalFun.transform(transform, false);
        GridFunction2D grid = originalFun.createDomainGrid(5);
        GridFunction2D gridTopt = grid.transform(transform, false);
        transform.changeParameters(thetaOriginal);
        GridFunction2D gridTorig = grid.transform(transform, false);
        return new GridFunction2D[]{originalFun, fMov, imgTopt, grid, gridTorig, gridTopt};
    }

}
