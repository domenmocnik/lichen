package examples;

import costfun.MutualInformation;
import gridfun.GridFunction2D;
import io.Image2D;
import optim.ConjugateGradientMethod;
import transform.Rigid2D;

import java.util.Arrays;

/**
 * Example for multiresolution rigid registration.
 */
public class RigidRegistration {

    public static void main(String[] args) {

        GridFunction2D f = Image2D.readLCH("/home/domen/IdeaProjects/lichen/res/mrslice.lch");
        GridFunction2D g = f.downsample();
        GridFunction2D h = g.downsample();

        Rigid2D transform = new Rigid2D(f.lengthX / 2, f.lengthY / 2);
        final double[] s =  transform.getPreconditionMatrix(f.lengthX, f.lengthY, 0, 0);

        // level 0:
        MutualInformation mi = new MutualInformation(h, h, transform, 32);
        ConjugateGradientMethod optimiser = new ConjugateGradientMethod(mi);
        double[] theta0 = new double[]{ Math.PI / 24, 8, -6 };
        double[] thetaOpt = optimiser.optimise(theta0, s, 8);
        System.out.println(Arrays.toString(thetaOpt));

        // level 1:
        mi = new MutualInformation(g, g, transform, 32);
        optimiser = new ConjugateGradientMethod(mi);
        thetaOpt = optimiser.optimise(thetaOpt, s, 8);
        System.out.println(Arrays.toString(thetaOpt));

        // level 2:
        mi = new MutualInformation(f, f, transform, 32);
        optimiser = new ConjugateGradientMethod(mi);
        thetaOpt = optimiser.optimise(thetaOpt, s, 8);
        System.out.println(Arrays.toString(thetaOpt));

    }

}
