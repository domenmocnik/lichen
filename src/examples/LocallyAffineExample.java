package examples;

import display.GridFunctionView;
import gridfun.BicubicInterpolator;
import gridfun.GridFunction2D;
import io.Image2D;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import transform.LocallyAffine2D;
import transform.Transformation2D;
import util.OutOfGridException;
import util.Point2D;

/**
 * Test for 2D locally affine transformation.
 */
public class LocallyAffineExample extends Application {

    @Override
    public void start(Stage primaryStage) {
        GridFunction2D f = Image2D.readLCH("/home/domen/IdeaProjects/lichen/res/mrslice.lch");
        GridFunctionView fView = new GridFunctionView(f);
        BicubicInterpolator fip = new BicubicInterpolator(f);

        Transformation2D transform = new LocallyAffine2D(x -> Math.atan(x) / x, x -> x);
        transform.changeParameters(new double[]{f.lengthX / 2, f.lengthY / 2, Math.PI / 4, 2, 1, 0, 0.05});

        GridFunction2D g = new GridFunction2D(f.sizeX, f.sizeY, f.spacingX, f.spacingY);
        int lib = 0;
        for (int i = 0; i < g.sizeX; i++) {
            for (int j = 0; j < g.sizeY; j++) {
                try {
                    Point2D p = transform.map(new Point2D(i * f.spacingX, j * f.spacingY));
                    g.values[lib] = fip.valueAt(p);
                } catch (OutOfGridException e) {
                    g.values[lib] = 0;
                }
                lib++;
            }
        }
        GridFunctionView gView = new GridFunctionView(g);

        Pane pane = new FlowPane(fView, gView);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.getIcons().add(new Image("file:/home/domen/IdeaProjects/lichen/res/lichen.png"));
        primaryStage.setTitle("Bark");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
