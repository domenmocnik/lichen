package examples;

import costfun.MutualInformation;
import display.GridFunctionView;
import gridfun.GridFunction2D;
import io.Image2D;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import optim.ConjugateGradientMethod;
import transform.Transformation2D;
import transform.Translation2D;

import java.util.Arrays;


/**
 * Test for multiresolution optimisation
 */
public class Multiresolution extends Application{

    @Override
    public void start(Stage primaryStage) {
        GridFunction2D f = Image2D.readLCH("/home/domen/IdeaProjects/lichen/res/mrslice.lch");
        GridFunctionView fView = new GridFunctionView(f);
        GridFunction2D g = f.downsample();
        GridFunctionView gView = new GridFunctionView(g);
        GridFunction2D h = g.downsample();
        GridFunctionView hView = new GridFunctionView(h);
        Pane pane = new FlowPane(fView, gView, hView);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.getIcons().add(new Image("file:/home/domen/IdeaProjects/lichen/res/lichen.png"));
        primaryStage.setTitle("Bark");
        primaryStage.show();

        // level 0:
        Transformation2D transform = new Translation2D();
        MutualInformation mi = new MutualInformation(h, h, transform, 32);
        ConjugateGradientMethod optimiser = new ConjugateGradientMethod(mi);
        double[] theta0 = new double[]{15, -8};
        double[] thetaOpt = optimiser.optimise(theta0);
        System.out.println(Arrays.toString(thetaOpt));

        // level 1:
        mi = new MutualInformation(g, g, transform, 32);
        optimiser = new ConjugateGradientMethod(mi);
        thetaOpt = optimiser.optimise(thetaOpt);
        System.out.println(Arrays.toString(thetaOpt));

        // level 2:
        mi = new MutualInformation(f, f, transform, 32);
        optimiser = new ConjugateGradientMethod(mi);
        thetaOpt = optimiser.optimise(thetaOpt);
        System.out.println(Arrays.toString(thetaOpt));
    }

    public static void main(String[] args) {
        launch(args);
    }
}
