package examples;

import costfun.MutualInformationFor3DRegistration;
import gridfun.GridFunction3D;
import io.Image2D;
import org.apache.commons.math3.optim.*;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;
import transform.LocallySimilarityTransform3D;
import transform.Transformation3D;
import util.Ops;

import java.util.Arrays;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;

/**
 * Example that demonstrates the registration of 3D images using locally similarity transformation.
 */
public class LocallySimilarityExample {

    private static final double G = Math.PI / 180; // one angle-degree in radians
    private static final DoubleUnaryOperator lambdaFun = x -> Math.atan(x) / x;
    private static final GridFunction3D originalFun = Image2D.read3DImageFromLCH(
            "/home/domen/IdeaProjects/lichen/res/mr_image.lch");
    private static final double centerX = originalFun.lengthX * 0.5;
    private static final double centerY = originalFun.lengthY * 0.5;
    private static final double centerZ = originalFun.lengthZ * 0.5;

    private static String stringify(double[] array) {
        return Arrays.stream(array).mapToObj(x -> String.format("%.4f", x)).collect(Collectors.joining(", "));
    }

    public static void rigidRegistration() {
        Transformation3D transform = new LocallySimilarityTransform3D(lambdaFun, null);
        // parameters of transformation θ = [c1, c2, c3, s, ϑ, φ, ψ, a]

        // create artificial reference image
        double[] thetaOriginal = new double[]{centerX, centerY, centerZ, 1.3, -90 * G, 90 * G, 30 * G, 0.6};
        System.out.println("Original theta: " + stringify(thetaOriginal));
        transform.changeParameters(thetaOriginal);
        GridFunction3D fRef = originalFun.transform(transform);

        // mutual information between artificial reference image and moving (original) image
        MutualInformationFor3DRegistration mi =
                new MutualInformationFor3DRegistration(fRef, originalFun, transform, 32);

        // initial guess
        double[] theta0 = new double[] {centerX + 8, centerY - 4, centerZ + 12, 1, 0, 90 * G, 0, 1};
        System.out.println("Initial theta:  " + stringify(theta0) + "; value: " + mi.getValueAt(theta0));

        // CMA-ES optimisation
        double stopFitness = Double.NEGATIVE_INFINITY;
        boolean isActiveCMA = true;
        int diagonalOnly = 0;
        int checkFeasibleCount = 1;
        RandomGenerator random = new Well44497b(); //Well19937c();
        boolean generateStatistics = false;
        SimplePointChecker<PointValuePair> convergenceChecker =
                new SimplePointChecker<>(1e-9, 1e-9);
        double[] lowerBounds = Ops.sub(theta0, new double[]{30, 30, 30, 0.75, 180 * G, 90 * G, 45 * G, 0.95});
        double[] upperBounds = Ops.sum(theta0, new double[]{30, 30, 30, 1, 180 * G, 90 * G, 45 * G, 1});
        SimpleBounds bounds = new SimpleBounds(lowerBounds, upperBounds);
        transform.changeParameters(theta0);
        //double[] precond = transform.getPreconditionMatrix(originalFun.lengthX, originalFun.lengthY, 200, 400);
        //double[] zigma = Ops.mul(1 / precond[0], precond);
        double[] zigma = new double[] {10, 10, 10, 0.3, 90 * G, 45 * G, 30 * G, 0.5};
        OptimizationData sigma = new CMAESOptimizer.Sigma(zigma);
        OptimizationData popSize = new CMAESOptimizer.PopulationSize(80);
        //(int) (4 + Math.floor(3 * Math.log(thetaOriginal.length))));
        ObjectiveFunction objFun = new ObjectiveFunction(mi::getValueAt);

        CMAESOptimizer optimiser = new CMAESOptimizer(6000, stopFitness, isActiveCMA, diagonalOnly,
                checkFeasibleCount, random, generateStatistics, convergenceChecker);
        PointValuePair solution = optimiser.optimize(new InitialGuess(theta0), objFun,
                GoalType.MINIMIZE, bounds, sigma, popSize, new MaxEval(15000));

        System.out.println("Optimal theta:  " + stringify(solution.getPoint()) + "; value: " + solution.getValue());
        mi.shutdown();
    }

    public static void main(String[] args) {
        rigidRegistration();
    }
}
