package gridfun;

import util.Interpolation;
import util.OutOfGridException;
import util.Point2D;

import java.util.Arrays;

/**
 * Cubic interpolator for grid function on the two-dimensional grid.
 */
public class BicubicInterpolator implements Interpolator2D {


    private final GridFunction2D gfun; // reference to the grid function, which we interpolate
    private final Array2D coefficients; // spline coefficients


    /**
     * Constructs bicubic interpolator for the given grid function.
     *
     * @param gridFunction Grid function to be interpolated.
     */
    public BicubicInterpolator(GridFunction2D gridFunction) {
        if (gridFunction == null) {
            throw new IllegalArgumentException("Grid function should not be NULL.");
        }
        this.gfun = gridFunction;
        this.coefficients = Interpolation.cubicBSplineTransform2D(gridFunction);
    }


    /**
     * Calculates interpolated value at a given point.
     * @param u Point to be mapped.
     * @return Interpolated value at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    public double valueAt(Point2D u) throws OutOfGridException {
        if (u.x < 0 || u.x > gfun.lengthX || u.y < 0 || u.y > gfun.lengthY) { throw new OutOfGridException(); }
        // localize x
        double r = u.x / gfun.spacingX;
        final int fromI = (u.x == gfun.lengthX) ? ((int) r) - 1 : (int) r;
        final double px = r - fromI;
        // localize y
        r = u.y / gfun.spacingY;
        final int fromJ = (u.y == gfun.lengthY) ? ((int) r) - 1 : (int) r;
        final double py = r - fromJ;
        // interpolation
        final double[] v = new double[4]; // here will be values of interpolations along second dimension
        int start = fromI * coefficients.sizeY + fromJ;
        for (int i = 0; i < 4; i++) {
            v[i] = Interpolation.deBoor3(py, Arrays.copyOfRange(coefficients.values, start, start + 4));
            start += coefficients.sizeY;
        }
        return Interpolation.deBoor3(px, v); // interpolation along first dimension
    }


    /**
     * Calculates interpolated value and gradient at a given point.
     * @param u Input point.
     * @return Interpolated value and partial derivatives at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    public double[] valueAndGradientAt(Point2D u) throws OutOfGridException {
        if (u.x < 0 || u.x > gfun.lengthX || u.y < 0 || u.y > gfun.lengthY) { throw new OutOfGridException(); }
        // localize x
        double r = u.x / gfun.spacingX;
        final int fromI = (u.x == gfun.lengthX) ? ((int) r) - 1 : (int) r;
        final double px = r - fromI;
        // localize y
        r = u.y / gfun.spacingY;
        final int fromJ = (u.y == gfun.lengthY) ? ((int) r) - 1 : (int) r;
        final double py = r - fromJ;
        // interpolation
        final double[] v = new double[4]; // here will be values of interpolations along second dimension
        final double[] dy = new double[4]; // here will be derivatives of interpolations along second dimension
        int start = fromI * coefficients.sizeY + fromJ;
        for (int i = 0; i < 4; i++) {
            double[] row = Arrays.copyOfRange(coefficients.values, start, start + 4);
            double[] vdPair = Interpolation.deBoor3withDerivative(py, row);
            v[i] = vdPair[0];
            dy[i] = vdPair[1];
            start += coefficients.sizeY;
        }
        final double[] result = new double[3]; // storage for value and both partial derivatives
        result[0] = Interpolation.deBoor3(px, v);
        double[] dx = { v[1] - v[0], v[2] - v[1], v[3] - v[2] };
        result[1] = Interpolation.deBoor2(px, dx) / gfun.spacingX;
        result[2] = Interpolation.deBoor3(px, dy) / gfun.spacingY;
        return result;
    }

    public GridFunction2D getGridFunction() {
        return this.gfun;
    }

}
