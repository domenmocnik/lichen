package gridfun;

/**
 * 3-dimensional array of double-precision floating points.
 */
public class Array3D {

    public final int sizeX; // number of rows in a slice
    public final int sizeY; // number of columns in a slice
    public final int sizeZ; // number of slices
    public final double[] values; // elements of array; this is considered a 3D array of size (sizeX x sizeY x sizeZ)
    // linear indexing (row-major) of [i, j, k] is lic = (i * sizeY + j) * sizeZ + k
    public final int strideX; // a stride to increase first index by one
    public final int strideY; // a stride to increase second index by one
    // strideZ = 1

    public Array3D(int sizeX, int sizeY, int sizeZ, double[] values) {
        if (sizeX <= 0 || sizeY <= 0 || sizeZ <= 0) {
            throw new IllegalArgumentException("Grid size must be positive.");
        }
        if (values.length != sizeX * sizeY * sizeZ) {
            throw new IllegalArgumentException("The number of provided values does not match grid size.");
        }
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.values = values;
        this.strideX = sizeY * sizeZ;
        this.strideY = sizeZ;
    }

    public Array3D(int sizeX, int sizeY, int sizeZ) {
        if (sizeX <= 0 || sizeY <= 0 || sizeZ <= 0) {
            throw new IllegalArgumentException("Grid size must be positive.");
        }
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.values = new double[sizeX * sizeY * sizeZ];
        this.strideX = sizeY * sizeZ;
        this.strideY = sizeZ;
    }

    public void setElement(int x, int y, int z, double v) {
        this.values[(x * this.sizeY + y) * this.sizeZ + z] = v;
    }

    public double getElement(int x, int y, int z) {
        return this.values[(x * this.sizeY + y) * this.sizeZ + z];
    }

    public Array2D getSlice(int fixedDimension, int slice) {
        if (fixedDimension == 0) {
            Array2D result = new Array2D(this.sizeY, this.sizeZ);
            int n = result.values.length;
            int indexNow = slice * n;
            for (int i = 0; i < n; i++) {
                result.values[i] = this.values[indexNow];
                indexNow++;
            }
            return result;
        } else if (fixedDimension == 1) {
            Array2D result = new Array2D(this.sizeX, this.sizeZ);
            int stride = this.sizeY * (this.sizeZ - 1);
            int indexNow = slice * this.sizeZ;
            int i = 0;
            for (int x = 0; x < this.sizeX; x++) {
                for (int z = 0; z < this.sizeZ; z++) {
                    result.values[i] = this.values[indexNow];
                    indexNow++;
                    i++;
                }
                indexNow += stride;
            }
            return result;
        } else if (fixedDimension == 2) {
            Array2D result = new Array2D(this.sizeX, this.sizeY);
            int n = result.values.length;
            int indexNow = slice;
            for (int i = 0; i < n; i++) {
                result.values[i] = this.values[indexNow];
                indexNow += this.sizeZ;
            }
            return result;
        } else {
            throw new IllegalArgumentException("Fixed dimension should be either 0, 1 or 2.");
        }
    }



    public double[] minAndMaxValue() {
        double min = Double.MAX_VALUE;
        double max = -Double.MAX_VALUE;
        for (double v : this.values) {
            if (min > v) { min = v; } else if (max < v) { max = v; }
        }
        return new double[]{min, max};
    }

    public double minValue() {
        double min = Double.MAX_VALUE;
        for (double v : this.values) {
            if (min > v) { min = v; }
        }
        return min;
    }

    public double maxValue() {
        double max = Double.MIN_VALUE;
        for (double v : this.values) {
            if (max < v) { max = v; }
        }
        return max;
    }

}
