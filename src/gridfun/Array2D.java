package gridfun;

import java.util.Arrays;

/**
 * 2-dimensional array of double-precision floating points.
 */
public class Array2D {

    public final int sizeX; // number of rows
    public final int sizeY; // number of columns
    public final double[] values; // elements of array; this is considered a 2D array of size (sizeX x sizeY)
    // linear indexing (row-major) of [i, j] is lib = i * sizeY + j

    public Array2D(int sizeX, int sizeY, double[] values) {
        if (sizeX <= 0 || sizeY <= 0) {
            throw new IllegalArgumentException("Grid size must be positive.");
        }
        if (values.length != sizeX * sizeY) {
            throw new IllegalArgumentException("The number of provided values does not match grid size.");
        }
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.values = values;
    }

    public Array2D(int sizeX, int sizeY) {
        if (sizeX <= 0 || sizeY <= 0) {
            throw new IllegalArgumentException("Grid size must be positive.");
        }
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.values = new double[sizeX * sizeY];
    }

    public void setElement(int x, int y, double v) {
        this.values[x * this.sizeY + y] = v;
    }

    public double getElement(int x, int y) {
        return this.values[x * this.sizeY + y];
    }

    public void setRow(int row, double[] rowValues) {
        System.arraycopy(rowValues, 0, this.values, row * this.sizeY, this.sizeY);
    }

    public void setColumn(int column, double[] columnValues) {
        int current = column;
        for (int i = 0; i < this.sizeX; i++) {
            this.values[current] = columnValues[i];
            current += this.sizeY;
        }
    }

    public double[] getRow(int row) {
        final int from = row * this.sizeY;
        return Arrays.copyOfRange(this.values, from, from + this.sizeY);
    }

    public double[] getColumn(int column) {
        double[] result = new double[this.sizeX];
        int current = column;
        for (int i = 0; i < this.sizeX; i++) {
            result[i] = this.values[current];
            current += this.sizeY;
        }
        return result;
    }

    public double[] getRowRange(int row, int colFrom, int length) {
        final int from = row * this.sizeY +  colFrom;
        return Arrays.copyOfRange(this.values, from, from + length);
    }

    public double[] getColumnRange(int column, int rowFrom, int length) {
        double[] result = new double[length];
        int current = rowFrom * this.sizeY + column;
        for (int i = 0; i < length; i++) {
            result[i] = this.values[current];
            current += this.sizeY;
        }
        return result;
    }

    public double[] minAndMaxValue() {
        double min = Double.MAX_VALUE;
        double max = -Double.MAX_VALUE;
        for (double v : this.values) {
            if (min > v) { min = v; } else if (max < v) { max = v; }
        }
        return new double[]{min, max};
    }

    public double minValue() {
        double min = Double.MAX_VALUE;
        for (double v : this.values) {
            if (min > v) { min = v; }
        }
        return min;
    }

    public double maxValue() {
        double max = Double.MIN_VALUE;
        for (double v : this.values) {
            if (max < v) { max = v; }
        }
        return max;
    }

    /* Private helper functions to assist downsampling */

    /**
     * Downsampling of 1-dimensional array with filter [1/4, 1/2, 1/4] and step 2.
     * @param row The row to be downsampled.
     * @return Downsampled row of length (row.length + 1) / 2.
     */
    protected static double[] weightRow(double[] row) {
        final int L = (row.length + 1) / 2;
        final boolean even = row.length % 2 == 0;
        double[] w = new double[L];
        w[0] = (row[0] + row[1]) / 2;
        int up = even ? L : L - 1;
        for (int i = 1; i < up; i++) {
            w[i] = (row[2 * i - 1] + row[2 * i + 1]) / 4 + row[2 * i] / 2;
        }
        if (!even) {
            w[up] = (row[2 * up] + row[2 * up - 1]) / 2;
        }
        return w;
    }

    /**
     * Downsampling by factor 2 with Gaussian bluring.
     * @return Downsampled and blured version of original array.
     */
    public Array2D downsample() {
        final int sx = (this.sizeX + 1) / 2;
        final int sy = (this.sizeY + 1) / 2;
        Array2D a = new Array2D(this.sizeX, sy);
        for (int i = 0; i < this.sizeX; i++) {
            double[] w = weightRow(this.getRow(i));
            a.setRow(i, w);
        }
        Array2D b = new Array2D(sx, sy);
        for (int j = 0; j < sy; j++) {
            double[] w = weightRow(a.getColumn(j));
            b.setColumn(j, w);
        }
        return b;
    }

}
