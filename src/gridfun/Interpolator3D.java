package gridfun;

import util.OutOfGridException;
import util.Point3D;

/**
 * An interface to represent interpolator for grid function on the three-dimensional grid.
 */
public interface Interpolator3D {

    /**
     * Calculates interpolated value at a given point.
     * @param u Point to be mapped.
     * @return Interpolated value at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    double valueAt(Point3D u) throws OutOfGridException;

    /**
     * Calculates interpolated value and gradient at a given point.
     * @param u Input point.
     * @return Interpolated value and partial derivatives at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    double[] valueAndGradientAt(Point3D u) throws OutOfGridException;
}
