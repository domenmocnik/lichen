package gridfun;

import util.OutOfGridException;
import util.Point3D;

/**
 * Linear interpolator for grid function on the three-dimensional grid.
 */
public class TrilinearInterpolator implements Interpolator3D {

    private final GridFunction3D gfun; // reference to the grid function, which we interpolate
    // coefficients of interpolation are the same as image values, so no additional array for coefficients is needed.

    /**
     * Constructs tricubic interpolator for the given grid function.
     *
     * @param gridFunction Grid function to be interpolated.
     */
    public TrilinearInterpolator(GridFunction3D gridFunction) {
        if (gridFunction == null) {
            throw new IllegalArgumentException("Grid function should not be NULL.");
        }
        this.gfun = gridFunction;
    }

    /**
     * Calculates interpolated value at a given point.
     * @param u Point to be mapped.
     * @return Interpolated value at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    public double valueAt(final Point3D u) throws OutOfGridException {
        if (u.x < 0 || u.x > gfun.lengthX || u.y < 0 || u.y > gfun.lengthY || u.z < 0 || u.z > gfun.lengthZ) {
            throw new OutOfGridException();
        }
        // localize x
        double r = u.x / gfun.spacingX;
        final int fromI = (u.x == gfun.lengthX) ? ((int) r) - 1 : (int) r;
        final double px = r - fromI;
        // localize y
        r = u.y / gfun.spacingY;
        final int fromJ = (u.y == gfun.lengthY) ? ((int) r) - 1 : (int) r;
        final double py = r - fromJ;
        // localize z
        r = u.z / gfun.spacingZ;
        final int fromK = (u.z == gfun.lengthZ) ? ((int) r) - 1 : (int) r;
        final double pz = r - fromK;
        // interpolation
        // in z-direction:
        int start = (fromI * gfun.sizeY + fromJ) * gfun.sizeZ + fromK;
        double v1 = gfun.values[start] + pz * (gfun.values[start + 1] - gfun.values[start]); // (i,j,k) and (i,j,k+1)
        start += gfun.strideY;
        double v2 = gfun.values[start] + pz * (gfun.values[start + 1] - gfun.values[start]); // (i,j+1,k) and (i,j+1,k+1)
        start += gfun.strideX;
        double v4 = gfun.values[start] + pz * (gfun.values[start + 1] - gfun.values[start]); // (i+1,j+1,k) and (i+1,j+1,k+1)
        start -= gfun.strideY;
        double v3 = gfun.values[start] + pz * (gfun.values[start + 1] - gfun.values[start]); // (i+1,j,k) and (i+1,j,k+1)
        // in y-direction:
        double w1 = v1 + py * (v2 - v1);
        double w2 = v3 + py * (v4 - v3);
        // in x-direction:
        return w1 + px * (w2 - w1);
    }

    /**
     * Calculates interpolated value and gradient at a given point.
     * @param u Input point.
     * @return Interpolated value and partial derivatives at the input point.
     * @throws OutOfGridException If the input point lies outside of the grid domain.
     */
    public double[] valueAndGradientAt(Point3D u) throws OutOfGridException {
        if (u.x < 0 || u.x > gfun.lengthX || u.y < 0 || u.y > gfun.lengthY || u.z < 0 || u.z > gfun.lengthZ) {
            throw new OutOfGridException();
        }
        // localize x
        double r = u.x / gfun.spacingX;
        final int fromI = (u.x == gfun.lengthX) ? ((int) r) - 1 : (int) r;
        final double px = r - fromI;
        // localize y
        r = u.y / gfun.spacingY;
        final int fromJ = (u.y == gfun.lengthY) ? ((int) r) - 1 : (int) r;
        final double py = r - fromJ;
        // localize z
        r = u.z / gfun.spacingZ;
        final int fromK = (u.z == gfun.lengthZ) ? ((int) r) - 1 : (int) r;
        final double pz = r - fromK;
        // interpolation
        // in z-direction:
        int start = (fromI * gfun.sizeY + fromJ) * gfun.sizeZ + fromK;
        double dz1 = gfun.values[start + 1] - gfun.values[start];
        double v1 = gfun.values[start] + pz * dz1; // (i,j,k) and (i,j,k+1)
        start += gfun.strideY;
        double dz2 = gfun.values[start + 1] - gfun.values[start];
        double v2 = gfun.values[start] + pz * dz2; // (i,j+1,k) and (i,j+1,k+1)
        start += gfun.strideX;
        double dz4 = gfun.values[start + 1] - gfun.values[start];
        double v4 = gfun.values[start] + pz * dz4; // (i+1,j+1,k) and (i+1,j+1,k+1)
        start -= gfun.strideY;
        double dz3 = gfun.values[start + 1] - gfun.values[start];
        double v3 = gfun.values[start] + pz * dz3; // (i+1,j,k) and (i+1,j,k+1)
        // in y-direction:
        double dy1 = v2 - v1;
        double w1 = v1 + py * dy1;
        double dy2 = v4 - v3;
        double w2 = v3 + py * dy2;
        // in x-direction:
        double dx = w2 - w1;
        final double[] result = new double[4]; // storage for value and all three partial derivatives
        result[0] = w1 + px * dx;
        result[1] = dx / gfun.spacingX;
        result[2] = (dy1 + px * (dy2 - dy1)) / gfun.spacingY;
        result[3] = ((dz1 + py * (dz2 - dz1)) + px * (dz3 + py * (dz4 - dz3)))/ gfun.spacingZ;
        return result;
    }

    public GridFunction3D getGridFunction() {
        return this.gfun;
    }

}
