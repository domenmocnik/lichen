package gridfun;

import util.OutOfGridException;
import util.Point2D;

/**
 * Linear interpolator for grid function on the two-dimensional grid.
 */
public class BilinearInterpolator implements Interpolator2D {

    private final GridFunction2D gfun; // reference to the grid function, which we interpolate
    // coefficients of interpolation are the same as image values, so no additional array for coefficients is needed.

    /**
     * Constructs bicubic interpolator for the given grid function.
     *
     * @param gridFunction Grid function to be interpolated.
     */
    public BilinearInterpolator(GridFunction2D gridFunction) {
        if (gridFunction == null) {
            throw new IllegalArgumentException("Grid function should not be NULL.");
        }
        this.gfun = gridFunction;
    }

    /**
     * Calculates interpolated value at a given point.
     * @param u Point to be mapped.
     * @return Interpolated value at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    public double valueAt(final Point2D u) throws OutOfGridException {
        if (u.x < 0 || u.x > gfun.lengthX || u.y < 0 || u.y > gfun.lengthY) { throw new OutOfGridException(); }
        // localize x
        double r = u.x / gfun.spacingX;
        final int fromI = (u.x == gfun.lengthX) ? ((int) r) - 1 : (int) r;
        final double px = r - fromI;
        // localize y
        r = u.y / gfun.spacingY;
        final int fromJ = (u.y == gfun.lengthY) ? ((int) r) - 1 : (int) r;
        final double py = r - fromJ;
        // interpolation
        int start = fromI * gfun.sizeY + fromJ;
        double v1 = gfun.values[start] + py * (gfun.values[start + 1] - gfun.values[start]);
        start += gfun.sizeY;
        double v2 = gfun.values[start] + py * (gfun.values[start + 1] - gfun.values[start]);
        return v1 + px * (v2 - v1);
    }

    /**
     * Calculates interpolated value and gradient at a given point.
     * @param u Input point.
     * @return Interpolated value and partial derivatives at the input point.
     * @throws OutOfGridException If the input point lies outside of the grid domain.
     */
    public double[] valueAndGradientAt(Point2D u) throws OutOfGridException {
        if (u.x < 0 || u.x > gfun.lengthX || u.y < 0 || u.y > gfun.lengthY) { throw new OutOfGridException(); }
        // localize x
        double r = u.x / gfun.spacingX;
        final int fromI = (u.x == gfun.lengthX) ? ((int) r) - 1 : (int) r;
        final double px = r - fromI;
        // localize y
        r = u.y / gfun.spacingY;
        final int fromJ = (u.y == gfun.lengthY) ? ((int) r) - 1 : (int) r;
        final double py = r - fromJ;
        // interpolation
        final double[] result = new double[3]; // storage for value and both partial derivatives
        int start = fromI * gfun.sizeY + fromJ;
        double dy1 = gfun.values[start + 1] - gfun.values[start];
        double v1 = gfun.values[start] + py * dy1;
        start += gfun.sizeY;
        double dy2 = gfun.values[start + 1] - gfun.values[start];
        double v2 = gfun.values[start] + py * (gfun.values[start + 1] - gfun.values[start]);
        double dx = v2 - v1;
        result[0] = v1 + px * dx;
        result[1] = dx / gfun.spacingX;
        result[2] = (dy1 + px * (dy2 - dy1)) / gfun.spacingY;
        return result;
    }

    public GridFunction2D getGridFunction() {
        return this.gfun;
    }

}
