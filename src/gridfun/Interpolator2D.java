package gridfun;

import util.OutOfGridException;
import util.Point2D;

/**
 * An interface to represent interpolator for grid function on the two-dimensional grid.
 */
public interface Interpolator2D {

    /**
     * Calculates interpolated value at a given point.
     * @param u Point to be mapped.
     * @return Interpolated value at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    double valueAt(Point2D u) throws OutOfGridException;

    /**
     * Calculates interpolated value and gradient at a given point.
     * @param u Input point.
     * @return Interpolated value and partial derivatives at the input point.
     * @throws OutOfGridException If input point lies outside of the grid domain.
     */
    double[] valueAndGradientAt(Point2D u) throws OutOfGridException;

    /**
     * Enum, used to represent the type of interpolation used.
     */
    enum Type {
        LINEAR, // represents bilinear interpolator
        CUBIC // represents bicubic interpolator
    }
}
