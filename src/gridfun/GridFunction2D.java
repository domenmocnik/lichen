package gridfun;

import transform.Transformation2D;
import util.OutOfGridException;
import util.Point2D;

import java.util.Arrays;

/**
 * Real-valued function defined on bounded regular 2-dimensional grid of points. Used to model 2-dimensional images.
 */
public class GridFunction2D extends Array2D {

    // Data for grid shape, position and orientation
    public final double spacingX; // pixel size in x-direction
    public final double spacingY; // pixel size in y-direction
    public final double lengthX; // length of the grid in x-direction; spacingX * (sizeX - 1)
    public final double lengthY; // length of the grid in y-direction; spacingY * (sizeY - 1)
    // TODO: add origin and orthogonal transformation

    /**
     * Constructs grid function from given data.
     * @param sizeX size of the first dimension.
     * @param sizeY size of the second dimension.
     * @param values values of function in grid points.
     * @param spacingX grid spacing along first dimension.
     * @param spacingY grid spacing along second dimension.
     */
    public GridFunction2D(int sizeX, int sizeY, double[] values, double spacingX, double spacingY) {
        super(sizeX, sizeY, values);
        if (spacingX <= 0 || spacingY <= 0) {
            throw new IllegalArgumentException("Grid spacing must be positive.");
        }
        this.spacingX = spacingX;
        this.spacingY = spacingY;
        this.lengthX = spacingX * (sizeX - 1);
        this.lengthY = spacingY * (sizeY - 1);
    }

    /**
     * Constructs grid function from given data with all values set to 0.
     * @param sizeX size of the first dimension.
     * @param sizeY size of the second dimension.
     * @param spacingX grid spacing along first dimension.
     * @param spacingY grid spacing along second dimension.
     */
    public GridFunction2D(int sizeX, int sizeY, double spacingX, double spacingY) {
        this(sizeX, sizeY, new double[sizeX * sizeY], spacingX, spacingY);
    }

    /**
     * Returns new grid function which is this grid function downsampled by factor 2 with Gaussian blurring.
     * @return Downsampled and blurred version of original grid function.
     */
    public GridFunction2D downsample() {
        final int sx = (this.sizeX + 1) / 2;
        final int sy = (this.sizeY + 1) / 2;
        Array2D a = new Array2D(this.sizeX, sy);
        for (int i = 0; i < this.sizeX; i++) {
            double[] w = weightRow(this.getRow(i));
            a.setRow(i, w);
        }
        GridFunction2D b = new GridFunction2D(sx, sy, this.spacingX * 2, this.spacingY * 2);
        for (int j = 0; j < sy; j++) {
            double[] w = weightRow(a.getColumn(j));
            b.setColumn(j, w);
        }
        return b;
    }

    public GridFunction2D transform(Transformation2D g, boolean smooth, double backgroundValue,
                                    int sizeX, int sizeY, double spacingX, double spacingY) {
        GridFunction2D fn = new GridFunction2D(sizeX, sizeY, spacingX, spacingY);
        Interpolator2D interpolator = smooth ?
                new BicubicInterpolator(this) :
                new BilinearInterpolator(this);
        double x = 0;
        double y = 0;
        int j = 1;
        for (int lib = 0; lib < fn.values.length; lib++) {
            try {
                fn.values[lib] = interpolator.valueAt(g.map(new Point2D(x, y)));
            } catch (OutOfGridException e) {
                fn.values[lib] = backgroundValue;
            }
            if (j < sizeY) {
                y += spacingY;
                j++;
            } else {
                y = 0;
                j = 1;
                x += spacingX;
            }
        }
        return fn;
    }

    public GridFunction2D transform(Transformation2D g, boolean smooth, double backgroundValue) {
        return transform(g, smooth, backgroundValue, this.sizeX, this.sizeY, this.spacingX, this.spacingY);
    }

    public GridFunction2D transform(Transformation2D g, boolean smooth) {
        return transform(g, smooth, this.minValue(), this.sizeX, this.sizeY, this.spacingX, this.spacingY);
    }

    public GridFunction2D transform(Transformation2D g, boolean smooth,
                                    int sizeX, int sizeY, double spacingX, double spacingY) {
        return transform(g, smooth, this.minValue(), sizeX, sizeY, spacingX, spacingY);
    }

    /**
     * Creates a grid function that is a grid representation of a domain of a given grid function. The returned grid
     * function shows black grid on a white background.
     *
     * @param squareSize The length of a square's side (given roughly in one unit of spacing)
     * @return A grid representation of this grid-function's domain.
     */
    public GridFunction2D createDomainGrid(int squareSize) {
        int sx = ((int) this.lengthX) + 1;
        int sy = ((int) this.lengthY) + 1;
        GridFunction2D grid = new GridFunction2D(
                sx, sy, this.lengthX / (sx - 1), this.lengthY / (sy - 1));
        Arrays.fill(grid.values, 1.0);
        for (int i = squareSize - 1; i < sx; i += squareSize) {
            for (int j = 0; j < sy; j++) {
                grid.setElement(i, j, 0);
            }
        }
        for (int j = squareSize - 1; j < sy; j += squareSize) {
            for (int i = 0; i < sx; i++) {
                grid.setElement(i, j, 0);
            }
        }
        return grid;
    }

}
