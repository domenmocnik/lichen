package gridfun;

import transform.Transformation3D;
import util.OutOfGridException;
import util.Point3D;

/**
 * Real-valued function defined on bounded regular 3-dimensional grid of points. Used to model 3-dimensional images.
 */
public class GridFunction3D extends Array3D {

    // Data for grid shape, position and orientation
    public final double spacingX; // pixel size in x-direction
    public final double spacingY; // pixel size in y-direction
    public final double spacingZ; // pixel size in z-direction
    public final double lengthX; // length of the grid in x-direction; spacingX * (sizeX - 1)
    public final double lengthY; // length of the grid in y-direction; spacingY * (sizeY - 1)
    public final double lengthZ; // length of the grid in z-direction; spacingZ * (sizeZ - 1)
    private Point3D[] gridPoints = null; // points on the grid domain of this grid-function

    /**
     * Constructs grid function from given data.
     * @param sizeX size of the first dimension.
     * @param sizeY size of the second dimension.
     * @param sizeZ size of the third dimension.
     * @param values values of function in grid points.
     * @param spacingX grid spacing along first dimension.
     * @param spacingY grid spacing along second dimension.
     * @param spacingZ grid spacing along third dimension.
     */
    public GridFunction3D(int sizeX, int sizeY, int sizeZ, double[] values,
                          double spacingX, double spacingY, double spacingZ) {
        super(sizeX, sizeY, sizeZ, values);
        if (spacingX <= 0 || spacingY <= 0 || spacingZ <= 0) {
            throw new IllegalArgumentException("Grid spacing must be positive.");
        }
        this.spacingX = spacingX;
        this.spacingY = spacingY;
        this.spacingZ = spacingZ;
        this.lengthX = spacingX * (sizeX - 1);
        this.lengthY = spacingY * (sizeY - 1);
        this.lengthZ = spacingZ * (sizeZ - 1);
    }

    /**
     * Constructs grid function from given data with all values set to 0.
     * @param sizeX size of the first dimension.
     * @param sizeY size of the second dimension.
     * @param sizeZ size of the third dimension.
     * @param spacingX grid spacing along first dimension.
     * @param spacingY grid spacing along second dimension.
     * @param spacingZ grid spacing along third dimension.
     */
    public GridFunction3D(int sizeX, int sizeY, int sizeZ,
                          double spacingX, double spacingY, double spacingZ) {
        this(sizeX, sizeY, sizeZ, new double[sizeX * sizeY * sizeZ], spacingX, spacingY, spacingZ);
    }

    /**
     * Creates the array of Point3D objects that represent points on the grid domain of this grid function.
     *
     * @return Array of points on the grid domain of this grid function. The ordering of points in returned array is
     * the same as in linear ordering of this grid function.
     */
    public Point3D[] getDomainPoints() {
        if (this.gridPoints == null) { // they have not been calculated yet, will be now
            this.gridPoints = new Point3D[this.values.length];
            int lic = 0;
            double x = 0;
            for (int i = 0; i < sizeX; i++) {
                double y = 0;
                for (int j = 0; j < sizeY; j++) {
                    double z = 0;
                    for (int k = 0; k < sizeZ; k++) {
                        this.gridPoints[lic] = new Point3D(x, y, z);
                        lic++;
                        z += spacingZ;
                    }
                    y += spacingY;
                }
                x += spacingX;
            }
        } // else: they are already calculated
        return this.gridPoints;
    }

    /**
     * Returns a new grid-function that is the transformed version of this one.
     *
     * @param g The transformation.
     * @param backgroundValue A value used in voxels that map outside of the grid.
     * @param sizeX Number of voxels in first dimension of the transformed grid-function.
     * @param sizeY Number of voxels in second dimension of the transformed grid-function.
     * @param sizeZ Number of voxels in third dimension of the transformed grid-function.
     * @param spacingX Voxel spacing in first dimension of the transformed grid-function.
     * @param spacingY Voxel spacing in second dimension of the transformed grid-function.
     * @param spacingZ Voxel spacing in third dimension of the transformed grid-function.
     * @return A new grid-function that is the transformed version of this one with given transformation g.
     */
    public GridFunction3D transform(Transformation3D g, double backgroundValue,
                                    int sizeX, int sizeY, int sizeZ,
                                    double spacingX, double spacingY, double spacingZ) {
        GridFunction3D fn = new GridFunction3D(sizeX, sizeY, sizeZ, spacingX, spacingY, spacingZ);
        Interpolator3D interpolator = new TrilinearInterpolator(this);
        int lic = 0;
        double x = 0;
        for (int i = 0; i < sizeX; i++) {
            double y = 0;
            for (int j = 0; j < sizeY; j++) {
                double z = 0;
                for (int k = 0; k < sizeZ; k++) {
                    try {
                        fn.values[lic] = interpolator.valueAt(g.map(new Point3D(x, y, z)));
                    } catch (OutOfGridException e) {
                        fn.values[lic] = backgroundValue;
                    }
                    lic++;
                    z += spacingZ;
                }
                y += spacingY;
            }
            x += spacingX;
        }
        return fn;
    }

    /**
     * Returns a new grid-function that is the transformed version of this one.
     *
     * @param g The transformation.
     * @return A new grid-function that is the transformed version of this one with given transformation g.
     * The grid domain of new grid-function will be the same as the grid domain of this grid-function. It uses
     * minimum value of this grid-function as background value.
     */
    public GridFunction3D transform(Transformation3D g) {
        return this.transform(g, this.minValue(), this.sizeX, this.sizeY, this.sizeZ,
                this.spacingX, this.spacingY, this.spacingZ);
    }

}
